import os
import settings

# EOS or local paths
if settings.USER == "bill-menta":
    # Local space for testing
    EOS_PERSISTENT_VOLUME = os.path.join(os.sep,"home","bill-menta","workspace","emuprogress","data","emuprogress") # local testing
    EOS_EMU_DIR = os.path.join(os.sep,"home","bill-menta","workspace","emuprogress",'emulsionData') # local testing
elif settings.USER == "bill-redhat":
    # Local space for testing
    EOS_PERSISTENT_VOLUME = os.path.join(os.sep,"home","bill-redhat","workspace","mounts","emuprogressMOUNT") # local testing
    EOS_EMURECOPLOTS_VOLUME = os.path.join(os.sep,"home","bill-redhat","workspace","emu_reco_plots") # local testing
    EOS_EMU_DIR = os.path.join(os.sep,"home","bill-redhat","workspace","emuprogress",'emulsionData') # local testing
elif settings.USER == "root":
    # Local space for testing
    EOS_PERSISTENT_VOLUME = os.path.join(os.sep,"root","emuprogress","data") # local testing
    EOS_EMU_DIR = os.path.join(os.sep,"root","emuprogress", 'emulsionData') # local testing
else:
    # EOS space
    EOS_PERSISTENT_VOLUME = os.path.join(os.sep, 'eos','user','e','emuplot','emuprogress') # Read + Write access by the service account with username: 'emuplot'
    EOS_EMU_DIR = os.path.join(os.sep, 'eos','experiment','sndlhc', 'emulsionData') # Read + Write(!) access by the service account with username: 'emuplot'
    EOS_EMURECOPLOTS_VOLUME = os.path.join(os.sep,'eos','user','s','snd2cern',"emu_reco_plots")

# Project paths
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.join(PROJECT_DIR, 'src')
DATA_DIR = os.path.join(PROJECT_DIR, 'data')
DOC_DIR = os.path.join(PROJECT_DIR, 'doc')
TEST_DIR = os.path.join(PROJECT_DIR, 'testdir')
EXPORT_PNG = os.path.join(DOC_DIR, 'howto_export.png')
FIND_PNG = os.path.join(DOC_DIR, 'howto_find.png')
EOS_IMPORT_HASH_TXT = os.path.join(EOS_PERSISTENT_VOLUME, "eos_hash.txt")
GETSYSTEMINFO_BATCH =os.path.join(EOS_PERSISTENT_VOLUME, "GetSystemInfo.bat") 
CHECKLOG_PS1 =os.path.join(EOS_PERSISTENT_VOLUME, "CheckLog.ps1") 
GECKODRIVER = '/usr/local/bin/geckodriver'
CERN_ELOG_URL = "https://sndlogbook.cern.ch:8080/"
QUALITY_CHEKS_DIR = os.path.join(EOS_PERSISTENT_VOLUME, "QualityChecks")
STATUS_FILENAME = "status.json"
USAGE_FILENAME = "usage_summary.html"
RESCAN_LIST = os.path.join(EOS_PERSISTENT_VOLUME,"rescan_list.csv")
RESCAN_HISTORY = os.path.join(EOS_PERSISTENT_VOLUME,"rescan_history.csv")
# CERN
CERN_DIR = os.path.join(EOS_PERSISTENT_VOLUME, 'CERN')
CERN_MICROSCOPES_DIR = os.path.join(CERN_DIR, 'MICROSCOPES')
CERN_UPLOAD_CSV = os.path.join(CERN_DIR, 'upload.csv')
CERN_ELOG_CSV = os.path.join(CERN_DIR, 'elog.csv')
CERN_EOS_CSV = os.path.join(CERN_DIR, 'eos.csv')

# BO
BO_DIR = os.path.join(EOS_PERSISTENT_VOLUME, 'BO')
BO_MICROSCOPES_DIR = os.path.join(BO_DIR, 'MICROSCOPES')
BO_ELOG_XLSX = os.path.join(BO_DIR, 'elog.xlsx')
BO_UPLOAD_CSV = os.path.join(BO_DIR, 'upload.csv')
BO_ELOG_CSV = os.path.join(BO_DIR, 'elog.csv')
BO_EOS_CSV = os.path.join(BO_DIR, 'eos.csv')

# NA
NA_DIR = os.path.join(EOS_PERSISTENT_VOLUME, 'NA')
NA_MICROSCOPES_DIR = os.path.join(NA_DIR, 'MICROSCOPES')
NA_UPLOAD_CSV = os.path.join(NA_DIR, 'upload.csv')
NA_ELOG_CSV = os.path.join(NA_DIR, 'elog.csv')
NA_EOS_CSV = os.path.join(NA_DIR, 'eos.csv')

# LEB
LEB_DIR = os.path.join(EOS_PERSISTENT_VOLUME, 'LEB')
LEB_MICROSCOPES_DIR = os.path.join(LEB_DIR, 'MICROSCOPES')
LEB_UPLOAD_CSV = os.path.join(LEB_DIR, 'upload.csv')
LEB_ELOG_CSV = os.path.join(LEB_DIR, 'elog.csv')
LEB_EOS_CSV = os.path.join(LEB_DIR, 'eos.csv')

# NAG
NAG_DIR = os.path.join(EOS_PERSISTENT_VOLUME, 'NAG')
NAG_MICROSCOPES_DIR = os.path.join(NAG_DIR, 'MICROSCOPES')
NAG_UPLOAD_CSV = os.path.join(NAG_DIR, 'upload.csv')
NAG_ELOG_CSV = os.path.join(NAG_DIR, 'elog.csv')
NAG_EOS_CSV = os.path.join(NAG_DIR, 'eos.csv')

# MERGED
MERGED_DIR = os.path.join(EOS_PERSISTENT_VOLUME, 'MERGED')
ELOG_CSV = os.path.join(MERGED_DIR, 'elog.csv')
EOS_CSV = os.path.join(MERGED_DIR, 'eos.csv')
DB = os.path.join(MERGED_DIR,'emu.db')

# LISTS
LIST_ELOG_CSV = [CERN_ELOG_CSV, NA_ELOG_CSV, BO_ELOG_CSV, NAG_ELOG_CSV, LEB_ELOG_CSV]
LIST_EOS_CSV = [CERN_EOS_CSV, NA_EOS_CSV, BO_EOS_CSV, NAG_EOS_CSV, LEB_EOS_CSV]
LAB_DIRS = {"CR":CERN_DIR, "NA":NA_DIR, "LEB":LEB_DIR, "BO":BO_DIR, "NAG":NAG_DIR, "All":MERGED_DIR}

# Image registry
DOCKER_IMAGE = "gitlab-registry.cern.ch/vacharit/emu_labs_status"