import pandas as pd
from src.utils import closest_date_index, sort_weeks, connect_with_retry, unique_column_values, close_connection
import settings
import streamlit as st
# Functions to query sql db and add selection boxes in streamlit application

# Select any date (closest past date is default)
def select_date(db):
    conn = connect_with_retry(db)
    dates = pd.read_sql_query("SELECT DISTINCT date FROM LabProgress", conn)["date"].tolist()
    close_connection(conn)
    index = closest_date_index(dates)
    date = st.selectbox('Select date (dd/mm/yyyy)', dates, index=index)
    return date

# Select any lab.
def select_lab(db, extra_options=None):
    conn = connect_with_retry(db)
    labs = pd.read_sql_query("SELECT DISTINCT lab FROM LabProgress", conn)["lab"].tolist()
    close_connection(conn)
    if extra_options:
        labs = extra_options + labs
    lab = st.selectbox('Select Lab', labs)
    return lab
def radio_lab(db, extra_options=None):
    labs = list(settings.LABORATORIES.values())
    if extra_options:
        labs = extra_options + labs
    lab = st.radio('Select Lab', labs, horizontal=True)
    return lab

# Select run for selected lab
def select_run(db, lab):
    conn = connect_with_retry(db)
    runs = pd.read_sql_query(f"SELECT DISTINCT run FROM LabProgress WHERE lab='{lab}'", conn)["run"].tolist()
    close_connection(conn)
    run = st.selectbox('Select Run', runs)
    return run

# Select wall for selected lab and run
def select_wall(db, lab, run):
    conn = connect_with_retry(db)
    walls = pd.read_sql_query(f"SELECT DISTINCT wall FROM LabProgress WHERE lab='{lab}' AND run='{run}'", conn)["wall"].tolist()
    close_connection(conn)
    wall = st.selectbox('Select Wall', walls)
    return wall

# Select wall for selected lab, run and wall
def select_brick(db, lab, run, wall):
    #conn = connect_with_retry(db)
    #bricks = pd.read_sql_query(f"SELECT DISTINCT brick FROM LabProgress WHERE lab='{lab}' AND run='{run}' AND wall='{wall}'", conn)["brick"].tolist()
    bricks = ["1","2","3","4"]
    #close_connection(conn)
    brick = st.selectbox('Select Brick', bricks)
    return wall

# Select and filter df
def dfselect(dfs, column):
    filtered_dfs=[]
    values=unique_column_values(dfs[0], column) # Unique column values taken from the first df in the input list
    selection = st.selectbox(f'Select {column}', ["All"]+values)
    for df in dfs:
        if selection!= "All":
            df = df[(df[column] == selection)]
        filtered_dfs.append(df)
    return filtered_dfs

def dfselect_mic(df):
    mics = unique_column_values(df, 'Microscope')
    mic = st.radio('Select mic', mics, horizontal=True)
    return mic

"""
def dfselect_week(df):
    weeks = unique_column_values(df, 'Week')
    weeks = sort_weeks(weeks)
    week = st.selectbox('Select Week', ["All"]+weeks)
    if week!= "All":
        df = df[(df['Week'] == week)]
    return df
def dfselect_run(df):
    runs = unique_column_values(df, 'Run')
    run = st.selectbox('Select Run', ["All"]+runs)
    if run!="All":
        df = df[(df['Run'] == run)]
    return df
def dfselect_wall(df):
    walls = unique_column_values(df, 'Wall')
    wall = st.selectbox('Select Wall', ["All"]+walls)
    if wall!="All":
        df = df[(df['Wall'] == wall)]
    return df
def dfselect_brick(df):
    run_wall_bricks = unique_column_values(df, 'Run_Wall_Brick')
    run_wall_brick = st.selectbox('Select Brick', ["All"]+run_wall_bricks)
    if run_wall_brick!="All":
        df = df[(df['Run_Wall_Brick'] == run_wall_brick)]
    return df"""

# Select run in ScanningEOS
def select_lab_scanning(db):
    conn = connect_with_retry(db)
    labs = pd.read_sql_query("SELECT DISTINCT lab FROM ScanningEOS", conn)["lab"].tolist()
    close_connection(conn)
    lab = st.selectbox('Select Lab', labs)
    return lab

def select_run_scanning(db, lab):
    conn = connect_with_retry(db)
    runs = pd.read_sql_query(f"SELECT DISTINCT run FROM ScanningEOS WHERE lab='{lab}'", conn)["run"].tolist()
    close_connection(conn)
    run = st.selectbox('Select Run', runs)
    return run

# Select wall for selected run in ScanningEOS
def select_wall_scanning(db, lab, run):
    conn = connect_with_retry(db)
    walls = pd.read_sql_query(f"SELECT DISTINCT wall FROM ScanningEOS WHERE lab='{lab}' AND run='{run}'", conn)["wall"].tolist()
    close_connection(conn)
    wall = st.selectbox('Select Wall', walls)
    return wall

# Select brick for selected run and wall in ScanningEOS
def select_brick_scanning(db, lab, run, wall):
    conn = connect_with_retry(db)
    bricks = pd.read_sql_query(f"SELECT DISTINCT brick FROM ScanningEOS WHERE lab='{lab}' AND run='{run}' AND wall='{wall}'", conn)["brick"].tolist()
    close_connection(conn)
    brick = st.selectbox('Select brick', bricks)
    return brick

# Select run in RecoEOS
def select_run_reco(db):
    conn = connect_with_retry(db)
    runs = pd.read_sql_query("SELECT DISTINCT run FROM RecoEOS", conn)["run"].tolist()
    close_connection(conn)
    run = st.selectbox('Select Run', runs)
    return run

# Select wall for selected run in RecoEOS
def select_wall_reco(db, run):
    conn = connect_with_retry(db)
    walls = pd.read_sql_query(f"SELECT DISTINCT wall FROM RecoEOS WHERE run='{run}'", conn)["wall"].tolist()
    close_connection(conn)
    wall = st.selectbox('Select Wall', walls)
    return wall

# Select brick for selected run and wall in RecoEOS
def select_brick_reco(db, run, wall):
    conn = connect_with_retry(db)
    bricks = pd.read_sql_query(f"SELECT DISTINCT brick FROM RecoEOS WHERE run='{run}' AND wall='{wall}'", conn)["brick"].tolist()
    close_connection(conn)
    brick = st.selectbox('Select brick', bricks)
    return brick