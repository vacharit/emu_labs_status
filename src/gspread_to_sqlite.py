import pandas as pd
import sqlite3
import sys
sys.path.append("..")
import paths
import utils

def gspread_to_sql():
    # Load spreadsheet
    xlsx = pd.ExcelFile(paths.GSPREAD)
    #db = paths.DB
    db = paths.CONVERTED_DB
    run_info_data = {}
    summary_run_info_data = {}

    # Read SummaryRunInfo sheet into a dataframe
    summary_df = xlsx.parse('SummaryRunInfo')
    # Read RunInfo sheets into a dictionary of dataframes
    run_info_dfs = {sheet_name: xlsx.parse(sheet_name) for sheet_name in xlsx.sheet_names if sheet_name != 'SummaryRunInfo'}

    # Create SQLite connection and cursor
    conn = sqlite3.connect(db)
    cursor = conn.cursor()

    # Create tables
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS SummaryRunInfo (
            run TEXT PRIMARY KEY,
            start TEXT,
            end TEXT,
            mass_kg REAL,
            luminosity_inv_fb REAL,
            emut_tot INT,
            emu_ntype INT,
            emu_stype INT,
            emu_to_na INT,
            emu_to_bo INT,
            emu_to_cr INT,
            emu_to_leb INT,
            assigned INT,
            scanned INT,
            linked INT,
            aligned INT
        );
    """)

    cursor.execute("""
        CREATE TABLE IF NOT EXISTS RunInfo (
            run TEXT,
            wall INTEGER,
            brick INTEGER,
            num_films INTEGER,
            emu_type TEXT,
            lab TEXT,
            PRIMARY KEY (run, wall, brick, num_films, emu_type, lab),
            FOREIGN KEY (run) REFERENCES SummaryRunInfo (run)
        );
    """)

    # Insert data into SummaryRunInfo table
    summary_df.to_sql('SummaryRunInfo', conn, if_exists='append', index=False)

    # Insert data into RunInfo table
    for run, df in run_info_dfs.items():
        df['run'] = run
        df.to_sql('RunInfo', conn, if_exists='append', index=False)
    # Commit changes and close connection
    conn.commit()
    conn.close()

gspread_to_sql()
