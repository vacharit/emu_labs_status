import streamlit as st
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from src.utils import connect_with_retry, close_connection

def visualize(db, table, x, y, key1, key2):
########################### OPEN CONNECTION #################################
    conn = connect_with_retry(db)
    # Retrieve all the values that the keys can take
    selections1 = pd.read_sql_query(f"SELECT DISTINCT {key1} FROM {table}", conn)[key1].tolist()
    selections2 = pd.read_sql_query(f"SELECT DISTINCT {key2} FROM {table}", conn)[key2].tolist()
    # and provide selection boxes for them
    selection1 = st.selectbox(f'Select {key1}', selections1, key=f"select1_{table}_{x}_{y}_{key1}_{key2}")
    selection2 = st.selectbox(f'Select {key2}', selections2, key=f"select2_{table}_{x}_{y}_{key1}_{key2}")
    # Fetch selected data
    df = pd.read_sql_query(f"SELECT {x}, {y} FROM {table} WHERE {key1} = '{selection1}' AND {key2} = '{selection2}'", conn)
    df = df.replace('', pd.NA) # replace empty strings with NANs
    df = df.fillna(0) # replace NANs with 0s
    close_connection() 
########################### CLOSE CONNECTION #################################
    
    # Option for Chart or Table
    display_option = st.radio(f"Display Option", ["Chart", "Table"], key=f"radio_{table}_{x}_{y}_{key1}_{key2}")
    fig, ax = plt.subplots()
    # Plotting or showing table based on selected option
    if display_option == "Chart":
        # Plot
        plt.bar(df[x], df[y])
        plt.xlabel(x)
        plt.ylabel(y)
        plt.title(f'{selection1} / {selection2}')
        st.pyplot(plt)
    else:
        # Table
        st.write(df)


def visualize_SummaryRunInfo(db):
########################### OPEN CONNECTION #################################
    conn = connect_with_retry(db)
    cursor = conn.cursor()
    # Query the data
    cursor.execute("SELECT run, assigned, scanned, linked, aligned FROM SummaryRunInfo")
    data = cursor.fetchall()
    # Extracting data for visualization
    runs = [row[0] for row in data]
    assigned = [row[1] if row[1] is not None else 0 for row in data]
    scanned = [row[2] if row[2] is not None else 0 for row in data]
    linked = [row[3] if row[3] is not None else 0 for row in data]
    aligned = [row[4] if row[4] is not None else 0 for row in data]
    close_connection(conn)
########################### CLOSE CONNECTION #################################
    
    # Option for Chart or Table
    display_option = st.radio("Display Option", ["Chart", "Table"], key="radio")
    if display_option == "Chart":
        # Multiselect to select data series
        selected_series = st.multiselect("Select data series to visualize:", 
                                         ["Assigned", "Scanned", "Linked", "Aligned"],
                                         ["Assigned", "Scanned", "Linked", "Aligned"])
        # Plotting the bar chart with increased size
        fig, ax = plt.subplots()
        index = np.arange(len(runs))
        bar_width = 0.2
        opacity = 0.8
        if "Assigned" in selected_series:
            rects1 = plt.barh(index, assigned, bar_width, alpha=opacity, color='b', label='Assigned')
            for i, v in enumerate(assigned):
                if v != 0:
                    plt.text(v, i, str(v), color='black', va='center', fontsize=10, fontweight='bold')
        if "Scanned" in selected_series:
            rects2 = plt.barh(index + bar_width, scanned, bar_width, alpha=opacity, color='g', label='Scanned')
            for i, v in enumerate(scanned):
                if v != 0:
                    plt.text(v, i + bar_width, str(v), color='black', va='center', fontsize=10, fontweight='bold')
        if "Linked" in selected_series:
            rects3 = plt.barh(index + 2*bar_width, linked, bar_width, alpha=opacity, color='r', label='Linked')
            for i, v in enumerate(linked):
                if v != 0:
                    plt.text(v, i + 2*bar_width, str(v), color='black', va='center', fontsize=10, fontweight='bold')
        if "Aligned" in selected_series:
            rects4 = plt.barh(index + 3*bar_width, aligned, bar_width, alpha=opacity, color='y', label='Aligned')
            for i, v in enumerate(aligned):
                if v != 0:
                    plt.text(v, i + 3*bar_width, str(v), color='black', va='center', fontsize=10, fontweight='bold')
        plt.xlabel('Number of Emulsion Films')
        plt.ylabel('Run ID')
        plt.title('Summary Run Info')
        plt.yticks(index + bar_width * 1.5, runs)
        plt.legend(loc='best')
        plt.gca().invert_yaxis()  # Invert the vertical axis
        plt.tight_layout()
        st.pyplot(fig)
    elif display_option == "Table":
        df = pd.DataFrame(data, columns=['Run ID', 'Assigned', 'Scanned', 'Linked', 'Aligned'])
        st.write(df)


def visualize_ScanningEOS(db):
    conn1 = connect_with_retry(db)
    cursor1 = conn1.cursor()
    # Execute a query to fetch the required data
    cursor1.execute("SELECT run_wall_brick, size FROM SummaryScanningEOS")
    # Process the data
    data = cursor1.fetchall()
    run_wall_brick = [row[0] for row in data]
    size = [row[1] for row in data]
    # Plot 
    plt.figure(figsize=(10, 6))
    plt.bar(run_wall_brick, size, color='blue')
    plt.xlabel('run_wall_brick')
    plt.ylabel('size (GB)')
    plt.title('Bar plot of size vs. run_wall_brick')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    st.pyplot(plt)
    cursor1.close()
    close_connection(conn1)

    conn2 = connect_with_retry(db)
    cursor2 = conn2.cursor()
    # Execute a query to fetch the required data
    cursor2.execute("SELECT run_wall_brick, count, rescans FROM SummaryScanningEOS")
    # Process the data
    data = cursor2.fetchall()
    run_wall_brick = [row[0] for row in data]
    count = [row[1] for row in data]
    rescans = [row[2] for row in data]
    # Perform element-wise subtraction of lists
    scans = [c - r for c, r in zip(count, rescans)]
    # Plot 
    plt.figure(figsize=(10, 6))
    plt.bar(run_wall_brick, scans, color='blue')
    plt.xlabel('run_wall_brick')
    plt.ylabel('scans')
    plt.title('Bar plot of scans vs. run_wall_brick')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    st.pyplot(plt)
    cursor2.close()
    close_connection(conn2)

def vizualize_RecoEOS(db, run, wall, brick):
    conn = connect_with_retry(db)
    query = f"""
    SELECT plates, size
    FROM RecoEOS
    WHERE run='{run}' AND wall='{wall}' AND brick='{brick}'
    """
    df = pd.read_sql_query(query, conn)
    close_connection(conn)
    
    if df.empty:
        st.warning("No data available for the selected run, wall, and brick.")
        return

    plates = df['plates'].values[0]
    size = df['size'].values[0]
    
    # Plotting
    fig, ax = plt.subplots(figsize=(10, 6))
    ax.barh(['Size'], [size], color='skyblue')
    ax.set_xlabel('Size')
    ax.set_title(f'Size for Run: {run}, Wall: {wall}, Brick: {brick}')
    
    # Add plates as legend
    plates_text = f'Plates: {plates}'
    ax.text(0.95, 0.01, plates_text, verticalalignment='bottom', horizontalalignment='right',
            transform=ax.transAxes, color='green', fontsize=12)
    
    st.pyplot(fig)


    