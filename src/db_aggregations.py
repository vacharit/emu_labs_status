import pandas as pd
import streamlit as st
from src.utils import connect_with_retry, close_connection
from datetime import datetime

# Call function to ensure ALL table aggregations are performed 
def aggregate_all(db):
    # TODO: 
    # checks if an aggregation is needed. If not skip to save time
    # --Pipeline--
    # LabProgress, RunInfo
        # Summary LabProgress
            # Summary_Weekly
                # Summary_Monthly
                # Summary_RunInfo

    ## --DO NOT CHANGE EXECUTION SEQUENCE--
    #aggregate_SummaryLabProgress(db)
    #aggregate_SummaryWeekly(db)
    #aggregate_SummaryMonthly(db)
    #aggregate_SummaryRunInfo(db)
    #aggregate_SummaryScanningEOS(db)
    st.success("Summary tables updated successfully.")


def aggregate_SummaryLabProgress(db):
    # -- PERFORM AFTER MODIFYING LabProgress table--
    conn = connect_with_retry(db)
    
    cursor1 = conn.cursor()
    # Fetch data from the LabProgress table and aggregate values for each lab
    cursor1.execute("""
        SELECT lab,
            SUM(assigned) AS total_assigned,
            SUM(scanned) AS total_scanned,
            SUM(linked) AS total_linked,
            SUM(aligned) AS total_aligned
        FROM LabProgress
        GROUP BY lab
    """)
    aggregated_data = cursor1.fetchall()
    cursor1.close()
    conn.commit()
    close_connection(conn)

    #conn = connect_with_retry(db)
    #cursor2 = conn.cursor()
    ## Insert or update aggregated values into the SummaryLabProgress table
    #for lab_data in aggregated_data:
    #    lab, assigned, scanned, linked, aligned = lab_data
    #    cursor2.execute("""
    #        INSERT OR REPLACE INTO SummaryLabProgress (lab, assigned, scanned, linked, aligned)
    #        VALUES (?, ?, ?, ?, ?)
    #    """, (lab, assigned, scanned, linked, aligned))
    ## Calculate sum for 'TOT' row
    #cursor2.close()
    #conn.commit()
    #close_connection(conn)

    conn = connect_with_retry(db)
    cursor3 = conn.cursor()
    cursor3.execute("""
        INSERT OR REPLACE INTO SummaryLabProgress (lab, assigned, scanned, linked, aligned)
        SELECT 'TOT', SUM(assigned), SUM(scanned), SUM(linked), SUM(aligned)
        FROM LabProgress
    """)
    cursor3.close()
    conn.commit()
    close_connection(conn)
    #st.success("SummaryLabProgress table updated successfully.")

def aggregate_SummaryWeekly(db):
    # -- PERFORM AFTER aggregate_SummaryLabProgress()--
    conn = connect_with_retry(db)

    cursor = conn.cursor()
    # Select distinct combinations of year, week, and run from LabProgress table
    cursor.execute("SELECT DISTINCT year, week, run FROM LabProgress")
    rows = cursor.fetchall()
    for row in rows:
        year, week, run = row
        # Calculate cumulative assigned, scanned, linked, and aligned for each year, week and run
        cursor.execute("""
            SELECT 
                SUM(assigned), 
                SUM(scanned), 
                SUM(linked), 
                SUM(aligned) 
            FROM 
                LabProgress 
            WHERE 
                year = ? AND week = ? AND run = ?
        """, (year, week, run))
        aggregated_data = cursor.fetchone()
        # Update SummaryWeekly table with calculated aggregated_data
        cursor.execute("""
            UPDATE SummaryWeekly 
            SET 
                assigned = ?, 
                scanned = ?, 
                linked = ?, 
                aligned = ? 
            WHERE 
                year = ? AND week = ? AND run = ?
        """, (aggregated_data[0], aggregated_data[1], aggregated_data[2], aggregated_data[3], year, week, run))
    cursor.close()
    close_connection(conn)
    #st.success("SummaryWeekly table updated successfully.")

    
def aggregate_SummaryRunInfo(db):
    # -- PERFORM AFTER aggregate_SummaryWeekly()--
    conn = connect_with_retry(db)
    # Retrieve distinct runs
    runs = pd.read_sql_query("SELECT DISTINCT run FROM SummaryWeekly", conn)["run"].tolist()
    
    cursor1 = conn.cursor()
    for run in runs:
        # Calculate sums for assigned, scanned, linked, and aligned
        cursor1.execute("""
            SELECT 
                SUM(assigned), 
                SUM(scanned), 
                SUM(linked), 
                SUM(aligned) 
            FROM 
                SummaryWeekly 
            WHERE 
                run = ?
        """, (run,))
        sums1 = cursor1.fetchone()
        # Update SummaryRunInfo
        cursor1.execute("""
            UPDATE SummaryRunInfo 
            SET 
                assigned = ?, 
                scanned = ?, 
                linked = ?, 
                aligned = ? 
            WHERE 
                run = ?
        """, (sums1[0], sums1[1], sums1[2], sums1[3], run))
    cursor1.close()

    # Create second cursor to potentially use multithreading later
    cursor2 = conn.cursor()
    for run in runs:
        # Calculate sum num_films
        cursor2.execute("""
            SELECT 
                SUM(num_films) 
            FROM 
                RunInfo 
            WHERE 
                run = ?
        """, (run,))
        emu_tot = cursor2.fetchone()[0]
        # Update SummaryRunInfo
        cursor2.execute("""
            UPDATE SummaryRunInfo 
            SET 
                emu_tot = ? 
            WHERE 
                run = ?
        """, (emu_tot, run))
    cursor2.close()

    # Create third cursor to potentially use multithreading later
    cursor3 = conn.cursor()
    for run in runs:
        # Calculate sum num_films
        cursor3.execute("""
            SELECT 
                SUM(num_films) 
            FROM 
                RunInfo 
            WHERE 
                run = ? AND emu_type = ?
        """, (run, 'S'))
        emu_ntype = cursor3.fetchone()[0]
        # Update SummaryRunInfo
        cursor3.execute("""
            UPDATE SummaryRunInfo 
            SET 
                emu_stype = ? 
            WHERE 
                run = ?
        """, (emu_ntype, run))
    cursor3.close()

    # Create fourth cursor to potentially use multithreading later
    cursor4 = conn.cursor()
    for run in runs:
        # Calculate sum num_films
        cursor4.execute("""
            SELECT 
                SUM(num_films) 
            FROM 
                RunInfo 
            WHERE 
                run = ? AND emu_type = ?
        """, (run, 'N'))
        emu_ntype = cursor4.fetchone()[0]
        # Update SummaryRunInfo
        cursor4.execute("""
            UPDATE SummaryRunInfo 
            SET 
                emu_ntype = ? 
            WHERE 
                run = ?
        """, (emu_ntype, run))
    cursor4.close()

    # Create fifth cursor to potentially use multithreading later
    cursor5 = conn.cursor()
    for run in runs:
        # Calculate sum num_films
        cursor5.execute("""
            SELECT 
                SUM(num_films) 
            FROM 
                RunInfo 
            WHERE 
                run = ? AND lab = ?
        """, (run, 'NA'))
        emu_to_na = cursor5.fetchone()[0]
        # Update SummaryRunInfo
        cursor5.execute("""
            UPDATE SummaryRunInfo 
            SET 
                emu_to_na = ? 
            WHERE 
                run = ?
        """, (emu_to_na, run))
    cursor5.close()

    # Create sixth cursor to potentially use multithreading later
    cursor6 = conn.cursor()
    for run in runs:
        # Calculate sum num_films
        cursor6.execute("""
            SELECT 
                SUM(num_films) 
            FROM 
                RunInfo 
            WHERE 
                run = ? AND lab = ?
        """, (run, 'CR'))
        emu_to_cr = cursor6.fetchone()[0]
        # Update SummaryRunInfo
        cursor6.execute("""
            UPDATE SummaryRunInfo 
            SET 
                emu_to_cr = ? 
            WHERE 
                run = ?
        """, (emu_to_cr, run))
    cursor6.close()

    # Create seventh cursor to potentially use multithreading later
    cursor7 = conn.cursor()
    for run in runs:
        # Calculate sum num_films
        cursor7.execute("""
            SELECT 
                SUM(num_films) 
            FROM 
                RunInfo 
            WHERE 
                run = ? AND lab = ?
        """, (run, 'BO'))
        emu_to_bo = cursor7.fetchone()[0]
        # Update SummaryRunInfo
        cursor7.execute("""
            UPDATE SummaryRunInfo 
            SET 
                emu_to_bo = ? 
            WHERE 
                run = ?
        """, (emu_to_bo, run))
    cursor7.close()

    # Create eighth cursor to potentially use multithreading later
    cursor8 = conn.cursor()
    for run in runs:
        # Calculate sum num_films
        cursor8.execute("""
            SELECT 
                SUM(num_films) 
            FROM 
                RunInfo 
            WHERE 
                run = ? AND lab = ?
        """, (run, 'LEB'))
        emu_to_leb = cursor8.fetchone()[0]
        # Update SummaryRunInfo
        cursor8.execute("""
            UPDATE SummaryRunInfo 
            SET 
                emu_to_leb = ? 
            WHERE 
                run = ?
        """, (emu_to_leb, run))
    cursor8.close()

    # Final step - calculate total. Do not implement this in parallel
    # Calculate sum for 'TOT' row
    # Get rowid of 'TOT' row to exclude it from the SUM. If not found raise error.
    cursorID = conn.cursor()
    cursorID.execute("SELECT rowid FROM SummaryRunInfo WHERE run = 'TOT'")
    row = cursorID.fetchone()
    if not row: # not found, raise error
        st.error("Row with run=TOT not found in SummaryRunInfo. Ensure the database has the correct structure. ")
        raise Exception("Row with run=TOT not found in SummaryRunInfo. Ensure the database has the correct structure. ")
    else:
        rowid_to_exclude = row[0]
        cursorID.execute("""
            INSERT OR REPLACE INTO SummaryRunInfo (run, start, end, mass_kg, luminosity_inv_fb, emu_tot, emu_ntype, emu_stype, emu_to_na, emu_to_bo, emu_to_cr, emu_to_leb, assigned, scanned, linked, aligned)
            SELECT 'TOT', '' , '' , SUM(mass_kg), SUM(luminosity_inv_fb), SUM(emu_tot), SUM(emu_ntype), SUM(emu_stype), SUM(emu_to_na), SUM(emu_to_bo), SUM(emu_to_cr), SUM(emu_to_leb), SUM(assigned), SUM(scanned), SUM(linked), SUM(aligned)
            FROM SummaryRunInfo
            WHERE rowid <> ?
        """, (rowid_to_exclude,))
    cursorID.close()
    conn.commit()
    close_connection(conn)
    #st.success("SummaryRunInfo table updated successfully.")

def aggregate_SummaryMonthly(db):
    # -- PERFORM AFTER aggregate_SummaryWeekly()--
    # Connect to the database
    conn = connect_with_retry(db)
    cursor = conn.cursor()
    
    # Retrieve data from SummaryWeekly
    cursor.execute("SELECT date, assigned, scanned, linked, aligned FROM SummaryWeekly")
    weekly_data = cursor.fetchall()

    # Initialize a dictionary to store monthly sums
    monthly_sums = {}

    # Iterate through weekly data and calculate monthly sums
    for row in weekly_data:
        date_str, assigned, scanned, linked, aligned = row
        date_obj = datetime.strptime(date_str, '%d/%m/%Y')
        month_year_key = date_obj.strftime('%m/%Y')

        if month_year_key not in monthly_sums:
            monthly_sums[month_year_key] = [assigned or 0, scanned or 0, linked or 0, aligned or 0]
        else:
            monthly_sums[month_year_key] = [sum(x) for x in zip(monthly_sums[month_year_key], [assigned or 0, scanned or 0, linked or 0, aligned or 0])]

    # Update or insert into SummaryMonthly
    for month_year, sums in monthly_sums.items():
        assigned, scanned, linked, aligned = sums
        cursor.execute("INSERT OR REPLACE INTO SummaryMonthly (month, assigned, scanned, linked, aligned) VALUES (?, ?, ?, ?, ?)",
                (month_year, assigned, scanned, linked, aligned))
    cursor.close()
    # Commit changes and close connection
    conn.commit()
    close_connection(conn)
    #st.success("SummaryMonthly table updated successfully.")


def aggregate_SummaryScanningEOS(db):
    # Connect to the SQLite database
    conn = connect_with_retry(db)
    cursor = conn.cursor()

    # Truncate the SummaryScanningEOS table to clear existing data
    cursor.execute("DELETE FROM SummaryScanningEOS")
    # Repopulate the SummaryScanningEOS table with the updated summary data
    cursor.execute("""
        INSERT OR REPLACE INTO SummaryScanningEOS
        SELECT
            run_wall_brick,
            run,
            wall,
            brick,
            lab,
            year_dir,
            COUNT(DISTINCT plate) AS count,
            SUM(size) AS size,
            SUM(CASE WHEN rescan = 1 THEN 1 ELSE 0 END) AS rescans
        FROM
            ScanningEOS
        GROUP BY
            run_wall_brick, run, wall, brick, lab, year_dir
    """)
    cursor.close()
    # Commit changes and close the connection
    conn.commit()
    close_connection(conn)
    #st.success("SummaryScanningEOS table updated successfully.")


