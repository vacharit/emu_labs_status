import streamlit as st
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd

# Visualization function for progress accross time
def ELOGtime(df, time_col='Week'):
    if time_col not in ['Day', 'Week', 'Month', 'Year']:
        raise ValueError("time_col must be either 'Week' or 'Month'")

    if time_col == 'Day':
            # Group by 'Day', 'Lab', and 'Run_Wall_Brick' and count entries
            count_df = df.groupby(['Day', 'Lab', 'Run_Wall_Brick']).size().reset_index(name='Count')
            count_df = count_df.sort_values(['Day', 'Lab', 'Run_Wall_Brick'])
            x_col = 'Day'
    
    # Prepare the DataFrame for plotting
    elif time_col == 'Week':
        # Convert the 'Week' column to datetime range start dates
        df['Week_Start'] = pd.to_datetime(df[time_col].str.split(' - ').str[0], format="%a %d %b %Y %H:%M:%S")
        # Group by 'Week_Start', 'Lab', and 'Run_Wall_Brick' and count entries
        count_df = df.groupby(['Week_Start', 'Lab', 'Run_Wall_Brick']).size().reset_index(name='Count')
        count_df = count_df.sort_values(['Week_Start', 'Lab', 'Run_Wall_Brick'])
        x_col = 'Week_Start'
    
    elif time_col == 'Month':
        # Convert 'Month' to a datetime object for proper sorting
        df['Month'] = pd.to_datetime(df[time_col], format='%Y %B')
        # Group by 'Month', 'Lab', and 'Run_Wall_Brick' and count entries
        count_df = df.groupby(['Month', 'Lab', 'Run_Wall_Brick']).size().reset_index(name='Count')
        count_df = count_df.sort_values(['Month', 'Lab', 'Run_Wall_Brick'])
        x_col = 'Month'

    elif time_col == 'Year':
        # Group by 'Year', 'Lab', and 'Run_Wall_Brick' and count entries
        count_df = df.groupby(['Year', 'Lab', 'Run_Wall_Brick']).size().reset_index(name='Count')
        count_df = count_df.sort_values(['Year', 'Lab', 'Run_Wall_Brick'])
        x_col = 'Year'

    # Create a stacked bar plot with different colors for each lab
    fig = px.bar(count_df, x=x_col, y='Count', color='Lab',
                 labels={x_col: time_col, 'Count': 'Entries', 'Lab': 'Lab'},
                 hover_data={'Count': True, 'Run_Wall_Brick': True},
                 title=f'Entries / {time_col} / Lab',
                 text='Count')

    # Update layout
    fig.update_layout(
        xaxis_tickangle=-45,
        xaxis=dict(title=time_col),
        yaxis=dict(title='Entries'),
        barmode='stack',
        legend_title='Lab'
    )
    # Enforce integer ticks for 'Year'
    if time_col == 'Year':
        fig.update_layout(xaxis=dict(tickmode='linear',tick0=int(count_df[x_col].min()),dtick=1))

    fig.update_traces(texttemplate='%{text}', textposition='inside', 
                      marker=dict(line=dict(color='rgba(0,0,0,0.15)', width=1)))
    
    st.plotly_chart(fig)

# Visualization function for both EOS and ELOG entries
def EOSvsELOG(dfELOG, dfEOS, dfDB):
    # Calculate counts for each unique value in 'Run_Wall_Brick' column for ELOG
    countsELOG = dfELOG['Run_Wall_Brick'].value_counts().reset_index()
    countsELOG.columns = ['Run_Wall_Brick', 'ELOG']
    
    # Calculate counts for each unique value in 'Run_Wall_Brick' column for EOS
    countsEOS = dfEOS['Run_Wall_Brick'].value_counts().reset_index()
    countsEOS.columns = ['Run_Wall_Brick', 'EOS']
    
    # Merge with dfDB to get num_films for each Run_Wall_Brick for ELOG
    mergedELOG = countsELOG.merge(dfDB, on='Run_Wall_Brick', how='left')
    mergedELOG['Percentage_ELOG'] = (mergedELOG['ELOG'] / mergedELOG['num_films']) * 100

    # Merge counts into one dataframe
    merged_df = pd.merge(mergedELOG, countsEOS, on='Run_Wall_Brick', how='outer').fillna(0)
    merged_df['Percentage_EOS'] = (merged_df['EOS'] / merged_df['num_films']) * 100

    # Create figure
    fig = go.Figure()

    for idx, row in merged_df.iterrows():
        # Determine which bar should be on top
        if row['ELOG'] > row['EOS']:
            fig.add_trace(go.Bar(
                x=[row['Run_Wall_Brick']],
                y=[row['ELOG']],
                name='ELOG',
                marker_color='#00FF00',  # Bright green
                opacity=0.7,
                legendgroup='ELOG', showlegend=idx==0,
                customdata=[row['Percentage_ELOG']],
                hovertemplate='Run_Wall_Brick: %{x}<br>ELOG: %{y}<br>Percentage ELOG: %{customdata:.2f}%<extra></extra>',
                text=f"{row['Percentage_ELOG']:.2f}%",  # Add percentage text
                textposition='inside',  # Position text inside the bar
            ))
            fig.add_trace(go.Bar(
                x=[row['Run_Wall_Brick']],
                y=[row['EOS']],
                name='EOS',
                marker_color='#0000FF',  # Bright blue
                opacity=0.7,
                legendgroup='EOS', showlegend=idx==0,
                customdata=[row['Percentage_EOS']],
                hovertemplate='Run_Wall_Brick: %{x}<br>EOS: %{y}<br>Percentage EOS: %{customdata:.2f}%<extra></extra>'
            ))
        else:
            fig.add_trace(go.Bar(
                x=[row['Run_Wall_Brick']],
                y=[row['EOS']],
                name='EOS',
                marker_color='#0000FF',  # Bright blue
                opacity=0.7,
                legendgroup='EOS', showlegend=idx==0,
                customdata=[row['Percentage_EOS']],
                hovertemplate='Run_Wall_Brick: %{x}<br>EOS: %{y}<br>Percentage EOS: %{customdata:.2f}%<extra></extra>'
            ))
            fig.add_trace(go.Bar(
                x=[row['Run_Wall_Brick']],
                y=[row['ELOG']],
                name='ELOG',
                marker_color='#00FF00',  # Bright green
                opacity=0.7,
                legendgroup='ELOG', showlegend=idx==0,
                customdata=[row['Percentage_ELOG']],
                hovertemplate='Run_Wall_Brick: %{x}<br>ELOG: %{y}<br>Percentage ELOG: %{customdata:.2f}%<extra></extra>',
                text=f"{row['Percentage_ELOG']:.2f}%",  # Add percentage text
                textposition='inside',  # Position text inside the bar
            ))

    # Add a dashed line at y=60
    fig.add_shape(type='line',
                  x0=-0.5, x1=len(merged_df)-0.5,  # x-axis spans the entire range of the x-axis
                  y0=60, y1=60,
                  line=dict(color="magenta", width=2, dash="dash"))

    # Update layout
    fig.update_layout(
        title='Entries / Brick',
        barmode='overlay',
        xaxis_title='Run_Wall_Brick',
        yaxis_title='Entries',
        legend_title='Entries',
        shapes=[dict(type='line',
                     x0=-0.5, x1=len(merged_df)-0.5,
                     y0=60, y1=60,
                     line=dict(color="magenta", width=2, dash="dash"))]
    )

    # Show the figure
    st.plotly_chart(fig)