import os
from datetime import datetime, timedelta
import sqlite3
import time
import paths
import csv
import io
import grp
import pandas as pd
import settings
import re
import numpy as np
import logging
import inspect
import hashlib
import matplotlib.colors as mcolors 
import matplotlib.cm as cm 


def get_importInfo():
    text = "Logbooks are being updated daily \n\n"
    text += "_Latest imports_:\n\n"
    for lab, lab_dir in paths.LAB_DIRS.items():
        if lab=='All' : continue
        lab_csv = os.path.join(lab_dir, 'elog.csv')
        modf_time = get_file_modified_time(lab_csv)
        text += f'{lab}: :blue[{modf_time}]\n'
    eos_modf_time = get_file_modified_time(paths.EOS_CSV)
    text += f"\n EOS: :blue[{eos_modf_time}]"
    return text
    
def is_valid_password(password, correct_password_hash):
    password_hash = hashlib.sha256(password.encode()).hexdigest()
    return password_hash == correct_password_hash

def generate_html_table_from_csv(elog_csv, eos_csv):
    # Read CSV files
    df1 = pd.read_csv(elog_csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
    df1.fillna('', inplace=True)  # Replace NaN with empty strings

    df2 = pd.read_csv(eos_csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
    df2.fillna('', inplace=True)

    # Identify missing Run_Wall_Brick values
    missing_values = df1[~df1['Run_Wall_Brick'].isin(df2['Run_Wall_Brick'])]
    print("Missing Run_Wall_Brick values in eos_csv:")
    print(missing_values['Run_Wall_Brick'].unique())

    # Count entries for Run_Wall_Brick in both CSVs
    counts1 = df1['Run_Wall_Brick'].value_counts().reset_index()
    counts1.columns = ['Run_Wall_Brick', 'SCANNED(ELOG)']
    df1 = pd.merge(df1, counts1, on='Run_Wall_Brick')

    counts2 = df2['Run_Wall_Brick'].value_counts().reset_index()
    counts2.columns = ['Run_Wall_Brick', 'TRANSFERED(EOS)']
    df2 = pd.merge(df2, counts2, on='Run_Wall_Brick')

    # Count rescanned plates (Rescan = True) grouped by Run_Wall_Brick
    rescanned_counts = (
        df1[df1['Rescan'] == True]  # Filter rows where Rescan is True
        .groupby('Run_Wall_Brick')
        .size()
        .reset_index(name='RESCANNED')
    )
    df1 = pd.merge(df1, rescanned_counts, on='Run_Wall_Brick', how='left')
    df1['RESCANNED'] = df1['RESCANNED'].fillna(0).astype(int)  # Replace NaN with 0 and convert to integer

    # Aggregate Microscope information for each Run_Wall_Brick
    Microscope_info = (
        df1.groupby('Run_Wall_Brick')['Microscope']
        .apply(lambda x: ', '.join(
            v for v, _ in sorted(x.value_counts().items(), key=lambda item: -item[1])  # Sort by count descending
        ))
        .reset_index()
        .rename(columns={'Microscope': 'MIC'})
    )
    df1 = pd.merge(df1, Microscope_info, on='Run_Wall_Brick', how='left')

    # Merge data from both CSVs on Run_Wall_Brick
    df1 = pd.merge(df1, df2[['Run_Wall_Brick', 'TRANSFERED(EOS)']], on='Run_Wall_Brick', how='left')

    # Create a new DataFrame with required columns
    table_data = df1[['Run', 'Wall', 'Brick', 'EmuType', 'Run_Wall_Brick', 'Lab', 'MIC', 'SCANNED(ELOG)', 'RESCANNED', 'TRANSFERED(EOS)']].copy()
    table_data['RECO'] = ''  # Blank RECO column
    table_data['ANALYSIS'] = ''  # Blank ANALYSIS column
    table_data = table_data.drop_duplicates(subset=['Run_Wall_Brick'])

    # Generate unique colors for RUNs
    unique_runs = table_data['Run'].unique()
    colors = list(mcolors.TABLEAU_COLORS.values())  # Use Tableau colors for distinct options
    run_colors = {run: colors[i % len(colors)] for i, run in enumerate(unique_runs)}

    # Generate HTML table
    html_table = """
    <table style="border-collapse: collapse; width: 100%; text-align: center;" border="1">
        <tr>
            <th rowspan="2" scope="col">RUN</th>
            <th rowspan="2" scope="col">WALL</th>
            <th scope="col">BRICK</th>
            <th scope="col">EMU</th>
            <th scope="col">RUN_WALL_BRICK</th>
            <th scope="col">LAB</th>
            <th scope="col">MIC</th>
            <th scope="col">SCANNED(ELOG)</th>
            <th scope="col">RESCANNED</th>
            <th scope="col">TRANSFERED(EOS)</th>
            <th scope="col">RECO</th>
            <th scope="col">ANALYSIS</th>
        </tr>
        <tr></tr>
    """
    
    for run, run_group in table_data.groupby('Run'):
        # Apply a unique color to the RUN cell
        run_color = run_colors[run]
        html_table += f"""
        <tr>
            <td rowspan="{len(run_group)}" style="background-color: {run_color};">{run}</td>
        """
        for wall, wall_group in run_group.groupby('Wall'):
            html_table += f"""
            <td rowspan="{len(wall_group)}">{wall}</td>
            """
            for _, row in wall_group.iterrows():
                # Custom SCAN value display for ELOG
                if row['SCANNED(ELOG)'] >= 57:
                    scan_value_elog = "✔️"
                    style_elog = "background-color: green; color: white;"
                else:
                    scan_value_elog = f"{row['SCANNED(ELOG)']}/57"
                    style_elog = ""

                # Custom SCAN value display for EOS
                if not pd.isna(row['TRANSFERED(EOS)']) and row['TRANSFERED(EOS)'] >= 57:
                    scan_value_eos = "✔️"
                    style_eos = "background-color: green; color: white;"
                elif not pd.isna(row['TRANSFERED(EOS)']):
                    scan_value_eos = f"{int(row['TRANSFERED(EOS)'])}/57"
                    style_eos = ""
                else:
                    scan_value_eos = "N/A"
                    style_eos = ""

                # Custom RESCANNED value display
                rescan_value = "-" if row['RESCANNED'] == 0 else row['RESCANNED']

                # Mark problematic Run_Wall_Brick
                problem_style = ""
                if pd.isna(row['TRANSFERED(EOS)']):
                    problem_style = "background-color: red; color: white;"

                html_table += f"""
                <td>{row['Brick']}</td>
                <td>{row['EmuType']}</td>
                <td style="{problem_style}">{row['Run_Wall_Brick']}</td>
                <td>{row['Lab']}</td>
                <td>{row['MIC']}</td>
                <td style="{style_elog}">{scan_value_elog}</td>
                <td>{rescan_value}</td>
                <td style="{style_eos}">{scan_value_eos}</td>
                <td>{row['RECO']}</td>
                <td>{row['ANALYSIS']}</td>
                </tr>
                """

    html_table += "</table>"
    html_table = "\n".join(line.strip() for line in html_table.splitlines() if line.strip())  # Remove spaces
    return html_table

def traverseEOS_scanningData(csv_path, search_path):
    lab_dirs = settings.LAB_EOS_DIRS
    year_dirs = settings.YEAR_EOS_DIRS

    with open(csv_path, mode='w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        
        csv_writer.writerow([
            'Modif Date', 'Scanning Path', 'Lab', 'Run', 'Wall', 'Brick', 
            'Plate', 'Microscope', 'Rescan', 'Run_Wall_Brick', 'Size', 'Year_dir'
        ])
        
        for root_dir, _, files in os.walk(search_path):  # Traverse downwards
            for file in files:
                if file == settings.TRACKS_FILE:
                    scanning_path = os.path.join(root_dir, file)
                    path_components = parse_scanning_path(scanning_path)
                    run = path_components["Run"]
                    wall = path_components["Wall"]
                    brick = path_components["Brick"]
                    plate = path_components["Plate"]
                    rescan = path_components["Rescan"]
                    mic = path_components["Microscope"]
                    run_wall_brick = path_components["Run_Wall_Brick"]
                    lab = match_ancestor_dir(root_dir, dirs_to_match=lab_dirs, final_ancestor_dir=search_path)  # Traverse upwards
                    lab = match_lab(lab)  # Return lab abbreviation
                    year_dir = match_ancestor_dir(root_dir, dirs_to_match=year_dirs, final_ancestor_dir=search_path)  # Traverse upwards
                    modif_date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(os.path.getmtime(scanning_path)))
                    size = os.path.getsize(scanning_path) / (1024 ** 3)  # in GB

                    csv_writer.writerow([
                        modif_date, scanning_path, lab, run, wall, brick, plate, mic, rescan, run_wall_brick, size, year_dir 
                    ])
    return csv_path

def split_csv(input_filepath):
    """
    Reads a CSV file and splits it into multiple CSVs based on the 'Lab' column,
    saving each subset into directories defined in LAB_DIRS with filename 'eos.csv'.
    
    Parameters:
    - input_filepath (str): Path to the input CSV file.
    
    Returns:
    - List[str]: A list of paths to the constructed CSV files.
    """
    print(f'Splitting... {input_filepath}')
    
    # List to store the paths of the constructed CSV files
    constructed_csv_paths = []

    # Read the original CSV file
    try:
        df = pd.read_csv(input_filepath, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
    except FileNotFoundError:
        print(f"Error: File '{input_filepath}' not found.")
        return constructed_csv_paths  # Return an empty list if the file is not found

    # Group by 'Lab' column
    grouped = df.groupby('Lab')

    # Iterate over each group and save to respective directories
    for lab, group in grouped:
        if lab in paths.LAB_DIRS:
            directory = paths.LAB_DIRS[lab]
        else:
            print(f"Warning: Lab '{lab}' does not exist in LAB_DIRS. Skipping...")
            continue  # Skip if the Lab is not in LAB_DIRS

        # Ensure the directory exists
        os.makedirs(directory, exist_ok=True)

        # Construct the full path for saving
        filename = os.path.join(directory, 'eos.csv')

        # Save group to CSV
        group.to_csv(filename, index=False)

        # Add the path to the constructed CSV list
        constructed_csv_paths.append(filename)

        print(f"Saved {filename}.")

    # Return the list of constructed CSV file paths
    return constructed_csv_paths

def match_lab(lab):
    if lab in settings.LABORATORIES:
        return settings.LABORATORIES[lab]
    elif lab in settings.LABORATORIES.values():
        return lab
    else:
        raise ValueError(f"Error: {lab} does not match any keys or values in the settings.LABORATORIES dictionary")

def check_column_consistency(dataframes):
    # Get column names from the first DataFrame
    reference_columns = set(dataframes[0].columns)
    
    # Check column names consistency across all DataFrames
    for df in dataframes[1:]:
        if set(df.columns) != reference_columns:
            return False
    return True

def db_to_df_runinfo(db):
    conn = connect_with_retry(db)
    query = "SELECT run, wall, brick, num_films FROM RunInfo"
    df = pd.read_sql_query(query, conn)
    df['Run_Wall_Brick'] = df['run'].astype(str) + '_W' + df['wall'].astype(str) + '_B' + df['brick'].astype(str)
    df = df[['Run_Wall_Brick', 'num_films']]
    close_connection(conn)
    return df

def db_to_df(db, table, lab, run, wall, brick):
    conn = connect_with_retry(db)
    query = f"SELECT run_wall_brick, size FROM {table} WHERE lab='{lab}' AND run='{run}' AND wall='{wall}' AND brick='{brick}'"
    df = pd.read_sql_query(query, conn)
    df = df[['run_wall_brick', 'size']]
    close_connection(conn)
    return df

def colored_circle(color):
    return f"""
    <div style="
        display: inline-block;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        background-color: {color};
        margin-right: 5px;">
    </div>
    """

def write_permissions(directory, file_extension):
    files_with_write_permission = []
    for root, dirs, filenames in os.walk(directory):
        for file in filenames:
            if file.endswith(file_extension):
                file_path = os.path.join(root, file)
                if os.access(file_path, os.W_OK):
                    files_with_write_permission.append(file_path)
    return files_with_write_permission

# Function to generate HTML for tooltips
def generate_tooltip_html(column_name, tooltip_text):
    return f'<span title="{tooltip_text}" style="border-bottom: 1px dotted; cursor: help;">{column_name}</span>'

def recent_scantimes(df):
    df = df.copy()

    # Convert 'Date' column to datetime format
    df['Date'] = pd.to_datetime(df['Date'], errors='coerce')

    # Drop rows where 'Date' is NaT after conversion
    df = df.dropna(subset=['Date'])

    # Sort the dataframe in descending order by 'Date'
    df_sorted = df.sort_values(by='Date', ascending=False).copy()

    # Drop rows where 'ScanTime' or 'Microscope' is missing
    df_sorted = df_sorted.dropna(subset=['ScanTime', 'Microscope'])

    # Filter out rows where 'Microscope' contains '|'
    df_sorted = df_sorted[~df_sorted['Microscope'].str.contains('|', regex=False)]

    # Drop duplicate rows based on the 'Microscope' column, keeping the latest scan
    df_latest = df_sorted.drop_duplicates(subset='Microscope', keep='first').copy()

    # Sort by 'ScanTime(h)' from smallest to largest
    df_latest = df_latest.sort_values(by='ScanTime(h)').copy()

    # Add 'ScanTime(s)' column
    df_latest['ScanTime(s)'] = df_latest['ScanTime']

    # Select relevant columns
    df_scantimes = df_latest[['Date', 'Microscope', 'Lab', 'ScanTime(s)', 'ScanTime(h)', 'Run_Wall_Brick']].copy()
    # Ensure ScanTime(h) is numeric and replace problematic values
    df_scantimes['ScanTime(h)'] = pd.to_numeric(df_scantimes['ScanTime(h)'], errors='coerce')

    # Replace NaN values with 1 to avoid division errors
    df_scantimes['ScanTime(h)'].fillna(1, inplace=True)

    # Replace infinite values with 1 to prevent crashes
    df_scantimes.replace([np.inf, -np.inf], 1, inplace=True)

    # Compute Feasible scans per week
    df_scantimes['Feasible'] = np.floor((24*7) / df_scantimes['ScanTime(h)'])

    # Handle NaN values before converting to integer
    df_scantimes['Feasible'].fillna(0, inplace=True)  # Prevent NaN issues
    df_scantimes['Feasible'] = df_scantimes['Feasible'].replace([np.inf, -np.inf], 0)  # Convert infinite to 0
    df_scantimes['Feasible'] = df_scantimes['Feasible'].astype(int)  # Convert safely

    # Compute Expected scans per week
    df_scantimes['Expected'] = np.floor(24 / df_scantimes['ScanTime(h)']) * 7
    df_scantimes['Expected'].fillna(0, inplace=True)  # Prevent NaN issues
    df_scantimes['Expected'] = df_scantimes['Expected'].replace([np.inf, -np.inf], 0)  # Convert infinite to 0
    df_scantimes['Expected'] = df_scantimes['Expected'].astype(int)  # Convert safely


    # Get last 7 days' data
    seven_days_ago = pd.Timestamp.now() - pd.Timedelta(days=7)
    print("seven_days_ago")
    print(seven_days_ago)
    #df_Last7d = df_sorted[df_sorted['Date'] >= seven_days_ago].copy() #with time accuracy
    df_Last7d = df_sorted[df_sorted['Date'].dt.date >= seven_days_ago.date()].copy() #compare only dates and no times
    print(df_Last7d)
    print("df_sorted.head(20)")  # Shows first 20 rows
    print(df_sorted.head(20))  # Shows first 20 rows
    # Count scans per microscope in the last 7 days
    scans_last_7d = df_Last7d.groupby('Microscope').size().rename('Last7d')

    # Count total scans per microscope (all time)
    total_scans = df_sorted.groupby('Microscope').size().rename('TotalScans')

    # Compute average scan time over all data
    avg_scan_time = df_sorted.groupby('Microscope')['ScanTime(h)'].mean().rename('AvgScanTimeAll(h)')

    # Compute average scan time over the last 7 days
    avg_scan_time_7d = df_Last7d.groupby('Microscope')['ScanTime(h)'].mean().rename('AvgScanTime7d(h)')

    # Merge everything into df_scantimes
    df_scantimes = df_scantimes.merge(avg_scan_time_7d, on='Microscope', how='left')
    df_scantimes = df_scantimes.merge(avg_scan_time, on='Microscope', how='left')
    df_scantimes = df_scantimes.merge(scans_last_7d, on='Microscope', how='left').fillna(0)
    df_scantimes = df_scantimes.merge(total_scans, on='Microscope', how='left')

    # Convert 'Last7d' to integer
    df_scantimes['Last7d'] = df_scantimes['Last7d'].astype(int)

    # Replace NaN in 'AvgScanTime7d(h)' with 'AvgScanTimeAll(h)' when no scans in last 7 days
    df_scantimes['AvgScanTime7d(h)'] = df_scantimes['AvgScanTime7d(h)'].fillna(df_scantimes['AvgScanTimeAll(h)'])

    return df_scantimes


def generate_leaderboards(csv_path):
    # Load the CSV data
    df = pd.read_csv(csv_path,na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
    df = df[df['Author'] != 'Vasileios Chariton']  # filter out myself
    
    # Process the overall leaderboard
    overall_leaderboard = df['Author'].value_counts().reset_index()
    overall_leaderboard.columns = ['Author', 'Entries']

    # Process the weekly leaderboard
    weekly_leaderboard = df.groupby(['Author', 'Week']).size().reset_index(name='Entries') # create Entries column
    weekly_microscopes = df.groupby(['Author','Week'])['Microscope'].apply(set).reset_index(name='Microscopes') # create Microscope column
    weekly_leaderboard = pd.merge(weekly_leaderboard, weekly_microscopes, on=['Author','Week'])
    weekly_leaderboard = weekly_leaderboard.loc[weekly_leaderboard.groupby('Week')['Entries'].idxmax()]
    weekly_leaderboard = weekly_leaderboard.sort_values(by='Entries', ascending=False).reset_index(drop=True)
    weekly_leaderboard['Week'] = weekly_leaderboard.pop('Week') # Move the 'Week' column to the end

    microscopes_leaderboard = pd.DataFrame({"Microscope": df["Microscope"], "ScanTime(h)": df["ScanTime(h)"], "Date": df["Date"]})
    microscopes_leaderboard = microscopes_leaderboard.sort_values(by="ScanTime(h)", na_position='last').reset_index(drop=True)

    # Return leaderboards as a dictionary
    leaderboards = {
        "Overall": overall_leaderboard,
        "Weekly": weekly_leaderboard,
        "Microscopes": microscopes_leaderboard
    }
    
    return leaderboards

# Function to apply styles
def highlight_leaderboards(s):
    return ['background-color: green']*len(s) if s.name < 3 else ['']*len(s)

# Function to match ancestor directories
# Traverses upwards until specific ancestor directory is met
def match_ancestor_dir(start_dir, dirs_to_match, final_ancestor_dir):
    current_dir = start_dir
    while not current_dir == final_ancestor_dir: # until final_ancestor_dir is met
        if os.path.basename(current_dir) in dirs_to_match:
            return os.path.basename(current_dir)
        current_dir = os.path.dirname(current_dir) # move up 
    return "Undefined"

def is_member_of_group(group_name):
    try:
        group_members = grp.getgrnam(group_name).gr_mem
        user = os.getenv('USER')
        username = os.getenv('USERNAME')
        return (user in group_members) or (username in group_members)
    except KeyError:
        return False
    
def sort_weeks(date_strings):
    def parse_week_string(date_string):
        # Split the date string to extract the first date
        start_date_str = date_string.split(' - ')[0]
        # Convert the start date string to a datetime object
        start_date = datetime.strptime(start_date_str, "%a %d %b %Y %H:%M:%S")
        return start_date
    
    # Sort the list of date strings based on the parsed start date in descending order
    sorted_weeks = sorted(date_strings, key=parse_week_string, reverse=True)
    return sorted_weeks

# Return index of date list for the closest (past) date
def closest_date_index(dates):
    today = datetime.today().date()
    index = None
    closest_difference = float('inf')
    for i, date_str in enumerate(dates):
        date = datetime.strptime(date_str, '%d/%m/%Y').date()
        difference = today - date
        if difference.days > 0 and difference.days < closest_difference:
            closest_difference = difference.days
            index = i
    return index

# Return most recent file
def latest(folder, file_pref, file_ext):
    files = [f for f in os.listdir(folder) if f.endswith(file_ext)]
    if len(files) == 1:  # only one file      
        file = os.path.join(folder, files[0])
    else:
        files = [f for f in files if f.startswith(file_pref) and f.endswith(file_ext)] # filter files that don't match
        timestamps = [datetime.strptime(f.split(file_pref)[1], "%Y-%m-%d_%H-%M-%S" + file_ext) for f in files]
        idx_most_recent = timestamps.index(max(timestamps))
        file = os.path.join(folder, files[idx_most_recent])
    return file

# Try to connect to the database multiple times 
def connect_with_retry(db, retries=5, delay=1):
    # Configure logging
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
    logger = logging.getLogger(__name__)

    for retry in range(retries):
        try:
            logger.info(f"Connecting to SQLite database at {db}. Caller function: {inspect.stack()[1].function}")
            conn = sqlite3.connect(db)
            logger.info(f"Connection established. Caller function: {inspect.stack()[1].function}")
            return conn
        except sqlite3.OperationalError as e:
            if 'database is locked' in str(e):
                logger.info(f"Database is locked retry:{retry}. Caller function: {inspect.stack()[1].function}")
                time.sleep(delay)
            else:
                raise
    raise sqlite3.OperationalError("Max retries reached")

def close_connection(conn):
    # Configure logging
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
    logger = logging.getLogger(__name__)
    if conn:
        conn.close()
        logger.info(f"Connection closed. Caller function: {inspect.stack()[1].function}")

# Return most recent run
def get_latest_run_name(db):
    conn = connect_with_retry(db)
    cursor = conn.cursor()
    cursor.execute("SELECT MAX(CAST(SUBSTR(run, 4) AS INTEGER)) FROM SummaryRunInfo")
    latest_run_number = cursor.fetchone()[0]
    close_connection(conn)
    new_run_number = latest_run_number + 1 if latest_run_number is not None else 0
    return f"RUN{new_run_number}"


def parse_scanning_path(scanning_path):
    path_components = {
        'Run': "",
        'Wall': "",
        'Brick': "",
        'Plate': "",
        'Run_Wall_Brick': "",
        'Rescan': False,
        'Microscope': ""
    }
    
    # Convert to upper case if SCANPATH_TO_UPPER is enabled
    if settings.SCANPATH_TO_UPPER:
        scanning_path = scanning_path.upper()

    # Replace separators (with forward slashes)
    if settings.SCANPATH_REPLACE_BACKSLASH:
        scanning_path = scanning_path.replace("\\\\", "/").replace("\\", "/")
    
    # Extract Run, Wall, and Brick
    match = settings.RUN_WALL_BRICK_PATTERN_STRICT.search(scanning_path)
    if match:
        path_components['Run'] = 'RUN' + match.group(1)  # Extract RUN
        path_components['Wall'] = 'W' + match.group(2)   # Extract Wall
        path_components['Brick'] = 'B' + match.group(3)  # Extract Brick
    
    # Extract Plate and Rescan from parent dirname
    #dirname = os.path.basename(os.path.dirname(scanning_path))  
    #match = settings.PLATE_PATTERN_STRICT.search(dirname)
    last_component = os.path.basename(scanning_path)
    match = settings.PLATE_PATTERN_STRICT.search(last_component)
    if match:
        path_components['Plate'] = 'P' + match.group(0).lstrip('P')  # Ensure 'P' prefix is present
    
    # Check if '_RESCAN' is in the last path component
    path_components['Rescan'] = "_RESCAN" in last_component.upper()
    # Extract Microscope info
    mic_match = settings.MIC_PATTERN.search(scanning_path)
    if mic_match:
        path_components['Microscope'] = mic_match.group(1).upper()  # Extract and normalize MIC value to uppercase
    
    # Construct Run_Wall_Brick
    path_components['Run_Wall_Brick'] = (
        path_components["Run"] + "_" +
        path_components["Wall"] + "_" +
        path_components["Brick"]
    )
    
    return path_components

def find_matching_directories(path, pattern):
    matching_dirs = []

    # Traverse the current directory
    for dirpath, dirnames, _ in os.walk(path):
        for dirname in dirnames:
            if pattern.match(dirname):
                matching_dirs.append(dirname)

    return matching_dirs

# Function to get size of directory
def get_directory_size(dirpath):
    total_size = 0
    for dirpath, _, filenames in os.walk(dirpath):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size

# Function to return readable comma and dash separated list of elements
# For example list = ["p035", "p039", "p021", "p013", "p038", "p003", "p011", "p026", "p037", "p029", "p002", "p031", "p041", "p010", "p012", "p022", "p015", "p024", "p005", "p023", "p025", "p007", "p027", "p036", "p004", "p040", "p006", "p008", "p018", "p034", "p028", "p017", "p009", "p001", "p030", "p016", "p014"]]
# Will return "p001-p018,p021-p031,p034-p041"
def comma_dash_seperated_list(list):

    # Return empty string if the list is empty
    if not list:
        return ""
    # Sort the list
    list.sort()
    
    # Initialize variables
    result = []
    start = list[0]
    end = list[0]

    for i in range(1, len(list)):
        current_plate = list[i]
        
        # Check if the current plate is consecutive to the previous plate
        if int(current_plate[1:]) == int(end[1:]) + 1:
            end = current_plate
        else:
            # Add the previous range to the result
            if start == end:
                result.append(start)
            else:
                result.append(f"{start}-{end}")
            start = current_plate
            end = current_plate
    
    # Add the last range to the result
    if start == end:
        result.append(start)
    else:
        result.append(f"{start}-{end}")
    
    return ",".join(result)

def parse_reco_path(reco_path):
    path_components = {
        'Run': "",
        'Wall': "",
        'Brick': "",
    }
    
    components = reco_path.split(os.sep)
    for i, part in enumerate(components):
        if settings.BRICK_PATTERN.match(part):
            path_components["Brick"] = "B" + part[-1]  # Last digit of the brick name
            path_components['Wall'] = "W" + part[-2]   # Second to last digit of the brick name
            break  # Exit loop after finding the brick pattern
        elif settings.RUN_PATTERN.match(part):
            path_components['Run'] = part

    return path_components

def save_file(uploaded_file, save_path):
    """
    Save the uploaded file to the specified path.

    Parameters:
    - uploaded_file: Uploaded file object.
    - save_path: str, the path where the file should be saved.

    Returns:
    - str: The file path where the file is saved.
    """
    # Ensure the directory exists
    os.makedirs(os.path.dirname(save_path), exist_ok=True)
    
    # Save the uploaded file to the specified location
    with open(save_path, 'wb') as output_file:
        output_file.write(uploaded_file.getbuffer())

def unique_column_values(df, column):
    """
    Returns a sorted list of unique values from a specified column in a DataFrame.

    Parameters:
        df (pandas.DataFrame): The DataFrame containing the column.
        df column (str): The name of the column from which unique values are extracted.

    Returns:
        list: A sorted list of unique values from the specified column.
    """
    values = df[column].dropna().unique().tolist()
    values.sort()
    return values

def unique_column_combinations(df, columns):
    """
    Returns the unique values for the subset of columns
    
    Parameters:
        df (pandas.DataFrame): The DataFrame containing the subset of columns.
        df columns (list): A subset list of the df columns for which the unique values are extracted.
    
    Returns:
        df: The filtered DataFrame with the unique values from the specified columns.
    """
    uniques_df = df.drop_duplicates(columns)
    uniques_df = uniques_df[columns]
    return uniques_df

def get_file_modified_time(file_path):
    """
    Get the last modified time of a file.

    Args:
    - file_path (str): Path to the file.

    Returns:
    - datetime.datetime: Last modified time as a datetime object.
    """
    try:
        # Get the modification time in seconds since the epoch
        modification_time = os.path.getmtime(file_path)
        
        # Convert the modification time to a datetime object
        modified_time = datetime.fromtimestamp(modification_time)
        formatted_time = modified_time.strftime('%d %B %Y, %H:%M')
        

        return formatted_time
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return None
    except Exception as e:
        print(f"Error occurred while getting file modification time: {e}")
        return None