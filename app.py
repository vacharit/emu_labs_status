import streamlit as st
from st_pages import add_page_title, get_nav_from_toml

st.set_page_config(layout="wide")

# Sidebar logo
st.sidebar.image("doc/logo_light.png", width=200)  

# Navigation
nav = get_nav_from_toml("pages.toml")
pg = st.navigation(nav)
add_page_title(pg)
pg.run()


# Contact Details & GitHub in Sidebar
st.sidebar.markdown("---")
st.sidebar.markdown(
    """
    <div style="text-align: center;">
        <p style="font-size: 16px;"><b>👤 Contact:</b> <a href="mailto:vasileios.chariton@cern.ch">vasileios.chariton@cern.ch</a></p>
        <p style="font-size: 16px;"><b>📂 GitHub:</b> <a href="https://gitlab.cern.ch/vacharit/emu_labs_status" target="_blank">emuprogress</a></p>
    </div>
    """,
    unsafe_allow_html=True
)
