import streamlit as st
import pandas as pd
import os
import paths, settings
from datetime import datetime

RESCAN_CSV = paths.RESCAN_LIST  # File to store rescan list
HISTORY_CSV = paths.RESCAN_HISTORY  # File to store history log

# Load rescan list from CSV
def load_rescan_list():
    """Load the rescan list from CSV, handling missing columns gracefully."""
    if not os.path.exists(RESCAN_CSV) or os.stat(RESCAN_CSV).st_size == 0:
        return []
    
    try:
        df = pd.read_csv(RESCAN_CSV, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)

        # Ensure required columns exist
        required_columns = {"quality_path", "run", "run_w_b", "lab", "microscope", "plates"}

        if not required_columns.issubset(df.columns):
            st.error("CSV file is missing required columns. It may need to be recreated.")
            return []

        return df.to_dict(orient="records")

    except pd.errors.EmptyDataError:
        return []

def save_rescan_list(rescan_list, removed_entries=None):
    """Save the updated rescan list and log removals."""
    df = pd.DataFrame(rescan_list, columns=["quality_path", "run", "run_w_b", "lab", "microscope", "plates"])
    df.to_csv(RESCAN_CSV, index=False)

    # Log removals in history
    if removed_entries:
        log_history(removed_entries, "Removed")
        
# Function to log history
def log_history(entries, action):
    """Log changes (Added/Removed) with timestamps."""
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    history_entries = [{**entry, "timestamp": timestamp, "action": action} for entry in entries]

    if os.path.exists(HISTORY_CSV):
        history_df = pd.read_csv(HISTORY_CSV)
        history_df = pd.concat([history_df, pd.DataFrame(history_entries)], ignore_index=True)
    else:
        history_df = pd.DataFrame(history_entries)

    history_df.to_csv(HISTORY_CSV, index=False)

# Function to load history
def load_history():
    """Load the history log from CSV, handling missing or empty files."""
    if not os.path.exists(HISTORY_CSV) or os.stat(HISTORY_CSV).st_size == 0:
        return pd.DataFrame()  # Return empty DataFrame if no history

    try:
        df = pd.read_csv(HISTORY_CSV, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)

        if df.empty:
            return pd.DataFrame()
        return df
    except pd.errors.EmptyDataError:
        return pd.DataFrame()


# Display `RUN` in **big letters**
rescan_list = load_rescan_list()

if rescan_list:
    st.write("### Runs marked for rescan by Lab and Brick:")
    
    # Group entries by RUN, then by lab and brick (RUN_W_B)
    grouped_entries = {}
    for entry in rescan_list:
        run = entry["run"]
        lab = entry["lab"]
        brick = entry["run_w_b"]

        if run not in grouped_entries:
            grouped_entries[run] = {}
        if lab not in grouped_entries[run]:
            grouped_entries[run][lab] = {}
        if brick not in grouped_entries[run][lab]:
            grouped_entries[run][lab][brick] = []

        grouped_entries[run][lab][brick].append(entry)

    # Dictionary to store selected entries for removal
    selected_entries = {}

    # Show Microscope info
    for run, labs in grouped_entries.items():
        st.markdown(f"## **{run}**")  # Show RUN in big letters
        
        for lab, bricks in labs.items():
            st.subheader(f"{lab}")

            for brick, entries in bricks.items():
                microscope = entries[0]["microscope"]  # Assume all have the same microscope
                st.write(f"**{microscope}**")  # Show microscope

                with st.expander(f"Brick: {brick}", expanded=True):
                    for entry in entries:
                        entry_text = entry['plates']
                        selected_entries[entry["quality_path"]] = st.checkbox(entry_text, key=f"{run}_{lab}_{brick}_{entry['quality_path']}")

    # Button to remove selected entries
    if st.button("Rescan completed!"):
        to_remove = [entry for entry in rescan_list if selected_entries.get(entry["quality_path"], False)]

        if to_remove:
            rescan_list = [entry for entry in rescan_list if entry not in to_remove]
            save_rescan_list(rescan_list, to_remove)  # Save & Log History
            st.success("Selected plates removed from the rescan list.")
            st.rerun()
        else:
            st.warning("No plates selected for removal.")

# Load history records
history_df = load_history()

# Display history if available
if not history_df.empty:
    st.write("### Recent History")
    
    # Show the last 10 actions (or fewer if less available)
    history_display = history_df.tail(10)

    # Display as a table
    st.dataframe(history_display)
