import os
import re
import streamlit as st
import pandas as pd
import paths, settings
from pathlib import Path
from datetime import datetime
from src import selections

# Define paths for Rescan List and History Log
BASE_DIR = paths.EOS_EMURECOPLOTS_VOLUME
RESCAN_CSV = paths.RESCAN_LIST  # File to store rescan list
HISTORY_CSV = paths.RESCAN_HISTORY  # File to store history log

# Load rescan list from CSV
def load_rescan_list():
    if not os.path.exists(RESCAN_CSV) or os.stat(RESCAN_CSV).st_size == 0:
        return []
    try:
        df = pd.read_csv(RESCAN_CSV, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
        return df.to_dict(orient="records") if not df.empty else []
    except pd.errors.EmptyDataError:
        return []

# Save rescan list to CSV & Log History
def save_rescan_list(rescan_list, new_entries=None):
    df = pd.DataFrame(rescan_list, columns=["quality_path", "run", "run_w_b", "lab", "microscope", "plates"])
    df.to_csv(RESCAN_CSV, index=False)
    
    if new_entries:
        log_history(new_entries, "Added")

# Log history of changes
def log_history(entries, action):
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    history_entries = [{**entry, "timestamp": timestamp, "action": action} for entry in entries]
    
    if os.path.exists(HISTORY_CSV):
        history_df = pd.read_csv(HISTORY_CSV)
        history_df = pd.concat([history_df, pd.DataFrame(history_entries)], ignore_index=True)
    else:
        history_df = pd.DataFrame(history_entries)
    
    history_df.to_csv(HISTORY_CSV, index=False)

# Get subdirectories
def get_subdirectories(directory):
    return sorted([d.name for d in Path(directory).iterdir() if d.is_dir()])

# Extract unique plate numbers and variants from PNG filenames
def get_plate_variants(directory):
    """Extracts unique plate numbers and their variants (e.g., '32', '32_rescan', '32_v2')."""
    plate_variants = {}
    pattern = re.compile(r".*?(\d+)(\S*)")  

    for file in Path(directory).glob("*.png"):
        match = pattern.search(file.name)
        if match:
            plate_variant = match.group(1)  # Base number
            if match.group(2):  # If there's a suffix, add it
                plate_variant += match.group(2)
            plate_variants[plate_variant] = str(file)  # Store full file path

    return plate_variants  # Dict {plate_variant: full_file_path}

# Select RUN
runs = get_subdirectories(BASE_DIR)
selected_run = st.selectbox("Select RUN", options=runs)

if selected_run:
    run_path = os.path.join(BASE_DIR, selected_run)
    run_w_bs = get_subdirectories(run_path)
    selected_run_w_b = st.selectbox("Select RUN_W_B", options=run_w_bs)

    if selected_run_w_b:
        brick_dir = os.path.join(run_path, selected_run_w_b)
        categories = get_subdirectories(brick_dir)  # Contains "thickness" and "mosaic"
        selected_category = st.selectbox("Select Category", options=categories)

        if selected_category:
            category_dir = os.path.join(brick_dir, selected_category)
            plate_variants = get_plate_variants(category_dir)  # {plate_variant: full_file_path}

            # Load rescan list and extract full paths already in the list
            rescan_list = load_rescan_list()
            existing_quality_paths = {e["quality_path"] for e in rescan_list}

            st.write("### Select Plates for Rescan:")

            selected_plates = {}
            for plate, image_path in sorted(plate_variants.items(), key=lambda x: (int(re.search(r"\d+", x[0]).group()), x[0])):
                already_in_rescan = image_path in existing_quality_paths
                is_rescan_plate = "_rescan" in plate  # Check if it's a _rescan plate
                disable_checkbox = already_in_rescan or is_rescan_plate

                # Tooltip message when hovering over disabled checkboxes
                tooltip_message = ""
                if already_in_rescan:
                    tooltip_message = "Already in the rescan list."
                elif is_rescan_plate:
                    tooltip_message = "Selecting rescans is disabled."

                col1, col2 = st.columns([3, 1])  # Image (3/4 width), Checkbox (1/4 width)

                with col1:
                    st.image(image_path, caption=f"Plate {plate}")

                with col2:
                    selected_plates[plate] = st.checkbox(
                        f"{plate}",
                        key=f"select_{plate}",
                        value=False,
                        disabled=disable_checkbox,
                        help=tooltip_message  # Tooltip appears when hovering over disabled checkboxes
                    )

            # **Move Lab & Microscope Selection Below Plates**
            st.write("### Select Lab & Microscope")

            # Select LAB
            db = paths.DB
            selected_lab = selections.radio_lab(db)

            selected_microscope = None
            if selected_lab:
                lab_dir = paths.LAB_DIRS[selected_lab]
                csv_path = os.path.join(lab_dir, "elog.csv")

                # Read CSV file
                try:
                    df = pd.read_csv(csv_path, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
                    selected_microscope = selections.dfselect_mic(df)  # Select microscope from database
                except FileNotFoundError:
                    st.warning(f"Error: File '{csv_path}' not found.")
                    selected_microscope = None

            # **TO-RESCAN Button Below Lab & Microscope**
            if st.button("TO-RESCAN"):
                if not selected_lab or not selected_microscope:
                    st.error("Please select a lab and a microscope before submitting.")
                else:
                    new_entries = [
                        {
                            "quality_path": plate_variants[plate],  # Full path
                            "run": selected_run,  # Store RUN info
                            "run_w_b": selected_run_w_b,
                            "lab": selected_lab,
                            "microscope": selected_microscope,  # Store microscope info
                            "plates": plate  # Only plate number or variant
                        }
                        for plate, selected in selected_plates.items() if selected and plate_variants[plate] not in existing_quality_paths
                    ]

                    if new_entries:
                        rescan_list.extend(new_entries)
                        save_rescan_list(rescan_list, new_entries)  # Save + Log History
                        st.success(f"Added {len(new_entries)} new plates.")
                    else:
                        st.warning("No new plates were selected for rescan.")

            if not selected_plates:
                st.info("All plates in this category have already been added to the rescan list.")
