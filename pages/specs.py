import streamlit as st
import settings
import pandas as pd
import paths
import os
from src.selections import radio_lab

def find_Systems(directory):

    results = {}
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith('SystemInfo.txt'):
                file_path = os.path.join(root, file)
                parent_directory = os.path.basename(root)
                results[parent_directory] = file_path
    return results

# Get all labs from db to select one
db = paths.DB
lab = radio_lab(db)
lab_dir = paths.LAB_DIRS[lab]
csv = os.path.join(lab_dir, "elog.csv")

systems = find_Systems(lab_dir)

if not systems:  
    st.warning(f"No registered systems for {lab}")
    st.info("To register a system, follow these steps:")
    
    st.markdown(
        f"""

        0. **Create a system directory (if there isn't) and move to it** in emuplot/emuprogress:
           ```bash
           e.g. mkdir {lab_dir}/MICROSCOPES/MIC1
           cd {lab_dir}/MICROSCOPES/MIC1

           If you have no access, contact vasileios.chariton@cern.ch
           ```
        1. **Get the registration script**:
            [GetSystemInfo.bat](https://gitlab.cern.ch/vacharit/emutasks/-/blob/bbcc33c09ad93bbfa53d86987dfef0dd937dd931/GetSystemInfo/GetSystemInfo.bat)
        2. **Run with the system ID as the only argument**:
           ```bash
           e.g. {os.path.basename(paths.GETSYSTEMINFO_BATCH)} MIC1
           ```
           This will generate a `SystemInfo.txt` file.
        3. **Send the generated file to the system directory that you created before**:
           ```bash
           e.g. scp SystemInfo.txt <username>@lxplus.cern.ch:{lab_dir}/MICROSCOPES/MIC1
           ```
        """
    )
    st.stop()

# Select System 
st.subheader("Select System")
system = st.radio(
    "System", systems.keys(),
    index=0, horizontal=True
)

# Read CSV file
try:
    df = pd.read_csv(csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
except FileNotFoundError:
    st.warning(f"Error: File '{csv}' not found.")
    st.stop()

systeminfo_path = systems[system] 

# Display system information
st.subheader("System Information")

if os.path.exists(systeminfo_path):
    try:
        with open(systeminfo_path, "r") as file:
            lines = file.readlines()

        system_info = {}
        nested_info = {}
        current_section = None

        # Parse line by line
        for line in lines:
            line = line.strip()

            # Skip empty lines
            if not line:
                continue

            # Detect the "System Information" section
            if line.startswith('"System Information"'):
                current_section = "nested"
                continue

            # Parse key-value pairs
            if current_section == "nested" and ":" in line:
                key, value = map(str.strip, line.split(":", 1))
                nested_info[key.strip('"')] = value.strip('"')
            elif ":" in line:
                key, value = map(str.strip, line.split(":", 1))
                system_info[key.strip('"')] = value.strip('"')

        # Display main system information
        st.write("### General System Info")
        st.table(pd.DataFrame(system_info.items(), columns=["Key", "Value"]))

        # Display nested system information
        if nested_info:
            st.write("### Detailed System Info")
            st.table(pd.DataFrame(nested_info.items(), columns=["Key", "Value"]))

    except Exception as e:
        st.error(f"Error processing the file: {e}")
else:
    st.warning(f"Error: File '{systeminfo_path}' not found.")
