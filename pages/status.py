import streamlit as st
import settings
import pandas as pd
import paths
import os
import json
from datetime import datetime
from PIL import Image
from src.selections import radio_lab
from src.utils import recent_scantimes, generate_tooltip_html, colored_circle
import plotly.io as pio

# Create a row with three checkboxes
show_status = st.checkbox("Show status", value=True)
show_usage = st.checkbox("Show usage (CPU|GPU|Network)", value=True)
show_quality = st.checkbox("Show quality", value=True)

# Paths and CSV loading
db = paths.DB
lab = radio_lab(db)
lab_dir = paths.LAB_DIRS[lab]
csv = os.path.join(lab_dir, "elog.csv")
microscopes_dir = os.path.join(lab_dir, "MICROSCOPES")

# Get latest scantimes
try:
    df = pd.read_csv(csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
except FileNotFoundError:
    st.warning(f"Error: File '{csv}' not found.")
    st.stop()

df_scantimes = recent_scantimes(df)
# Iterate only over direct subdirectories of `microscopes_dir`
for mic_name in sorted(os.listdir(microscopes_dir)):  
    mic_path = os.path.join(microscopes_dir, mic_name)  # Full path to microscope directory

    # Ensure it's a directory (skip files)
    if not os.path.isdir(mic_path):
        continue

    status_file = os.path.join(mic_path, paths.STATUS_FILENAME)
    usage_file = os.path.join(mic_path, paths.USAGE_FILENAME)
    quality_files = [f for f in os.listdir(mic_path) if f.endswith('.png')]

    # Load and display STATUS if "Show status" is checked
    if show_status and os.path.exists(status_file):
        with open(status_file, "r", encoding="utf-8-sig") as f:
            status_data = json.load(f)

        ip_address = status_data["IPAddress"]
        time_taken = status_data["LatestScanningTime"]["TimeTaken"]
        scanning_time_s = status_data["LatestScanningTime"]["ScanningPeriod"] / 1000
        scanning_time_h = status_data["LatestScanningTime"]["ScanningPeriod_h"]
        log_content = "\n".join(status_data["LogContent"])
        st.markdown(f"### `{mic_name}` {ip_address}")

        # Display status information in a table
        timestamp = status_data["Timestamp"]["DateTime"]
        inactivity_duration_seconds = status_data["InactivityReport"]["InactivityDuration_s"]
        inactivity_duration_hours = inactivity_duration_seconds / 3600  # Convert to hours

        table_html = f"""
        <table style="width: 100%; border-collapse: collapse; text-align: left;">
            <tr><td><strong>Updated</strong></td><td>{timestamp}</td></tr>
            <tr><td><strong>Inactive (hours)</strong></td><td>{inactivity_duration_hours:.2f} h</td></tr>
            <tr><td><strong>Scanning (hours)</strong></td><td>{scanning_time_h:.2f} h</td></tr>
        </table>
        """
        st.markdown(table_html, unsafe_allow_html=True)

        # Show log if selected
        if st.checkbox(f"Show log for `{mic_name}`"):
            st.text_area("**Log:**", log_content, height=200)

        # Progress bar logic
        latest_scanning_time_row = df_scantimes[df_scantimes['Microscope'] == mic_name.lower()]
        if not latest_scanning_time_row.empty and 'ScanTime(s)' in latest_scanning_time_row:
            latest_scanning_time = latest_scanning_time_row['ScanTime(s)'].values[0]
            latest_scanning_time = float(latest_scanning_time) if isinstance(latest_scanning_time, str) else latest_scanning_time
            progress = min(scanning_time_s / latest_scanning_time, 1.0)

            if inactivity_duration_hours > 24:
                stuck_status = colored_circle("black")
                st.markdown(f"<p>{stuck_status} Inactive</p>", unsafe_allow_html=True)
            elif time_taken == "Unknown":
                if inactivity_duration_seconds > 600:
                    stuck_status = colored_circle("red")
                    st.progress(progress)
                    st.markdown(f"<p>{stuck_status} Stuck</p>", unsafe_allow_html=True)
                    st.write(f"Progress estimate: {progress * 100:.2f}%")
                else:
                    running_status = colored_circle("yellow")
                    st.markdown(f"<p>{running_status} Running</p>", unsafe_allow_html=True)
                    st.progress(progress)
                    st.write(f"Progress estimate: {progress * 100:.2f}%")
                    remaining_time_s = (1 - progress) * latest_scanning_time
                    remaining_time_h = remaining_time_s / 3600
                    remaining_time_display = f"{remaining_time_h:.2f} hours" if remaining_time_h >= 1 else f"{remaining_time_s / 60:.0f} minutes"
                    st.markdown(
                        f"""<div style="border: 2px solid #4CAF50; padding: 10px; border-radius: 10px; display: inline-block;">
                            <strong>Estimated remaining time:</strong> 
                            <span style="color:#4CAF50; font-size:20px;">{remaining_time_display}</span>
                        </div>""",
                        unsafe_allow_html=True
                    )
            else:
                finished_status = colored_circle("green")
                time_taken_hours = round(time_taken / 3600000, 2)
                st.markdown(f"<p>{finished_status} Finished: {time_taken_hours} h</p>", unsafe_allow_html=True)
                st.progress(1.0)
        else:
            st.warning(f"No scanning time available for microscope: {mic_name}")

    # Load and display Plotly usage summary if "Show usage" is checked
    if show_usage and os.path.exists(usage_file):
        try:
            with open(usage_file, "r", encoding="utf-8") as f:
                usage_html = f.read()

            st.markdown(f"#### **System Usage Summary for `{mic_name}`**")
            st.components.v1.html(usage_html, height=1000, scrolling=False)  # Ensure full display without scrolling

        except Exception as e:
            st

# Round, restructure 
df_scantimes = df_scantimes.round(2)
desired_column_order = [
    'Date', 'Microscope', 'Run_Wall_Brick', 'Expected', 'Last7d',
    'AvgScanTime7d(h)', 'AvgScanTimeAll(h)', 'TotalScans'
]
df_display = df_scantimes[desired_column_order].copy()

# Styling
def alternate_row_colors(index):
    colors = ["#264653", "#2A9D8F"]  # Vibrant alternating blue-green shades
    return f"background-color: {colors[index % len(colors)]}; color: white;"
def highlight_scantime(val):
    """Color ScanTime cells based on time range."""
    if val > 12:
        return "background-color: #D90429; color: white;"  # Deep Red for long scans
    elif val > 9:
        return "background-color: #FF7F50; color: black;"  # Deep Orange for medium-long scans
    else:
        return "background-color: #2FA84F; color: black;"  # Deep Green for fast scans
def highlight_scans(val):
    """Color Last7d based on scan count."""
    if val >= 5:
        return "background-color: #2FA84F; color: black;"  # Green for high activity
    elif val > 0:
        return "background-color: #FF7F50; color: black;"  # Orange for moderate activity
    else:
        return "background-color: #D90429; color: white;"  # Red for no recent scans

# Apply Alternating Row Colors First
styled_df = df_display.style.apply(lambda df: [alternate_row_colors(i) for i in range(len(df))], axis=0)
# Then Apply ScanTime and Activity Highlighting
styled_df = styled_df.applymap(highlight_scantime, subset=[ 'AvgScanTime7d(h)']) \
                     .applymap(highlight_scans, subset=['Last7d'])
# Apply Formatting (No extra zeros)
styled_df = styled_df.format({
    'ScanTime(h)': '{:.2f}',
    'AvgScanTime7d(h)': '{:.2f}',
    'AvgScanTimeAll(h)': '{:.2f}'
})
# Display
st.subheader("📊 Weekly Status")
st.dataframe(styled_df, use_container_width=True)
