import streamlit as st
import settings
import pandas as pd
import paths
import os
import re
from datetime import datetime
from src.selections import radio_lab, dfselect_mic
from src.utils import get_importInfo

def get_lastEntry(df, selected_microscope):
    """
    Fetch the last row filtered by the selected microscope. If no data exists, return an empty dictionary.
    """
    # Filter dataframe by microscope
    filtered_df = df[df['Microscope'] == selected_microscope]
    last_row = None

    if not filtered_df.empty:
        last_row = filtered_df.iloc[-1]
    elif not df.empty:
        last_row = df.iloc[-1]

    if last_row is not None:
        # Replace NaN values with defaults
        last_row = last_row.fillna(0)  # Or use specific defaults as needed

    return last_row.to_dict() if last_row is not None else {}

def calculate_thickness(mic_glass, mic_bot, mic_base, mic_top):
    """
    Calculate thickness values dynamically.
    """
    glass = mic_glass
    bottom = mic_bot - mic_glass
    base = mic_base - mic_bot - mic_glass
    top = mic_top - mic_base
    return glass, bottom, base, top


st.caption(get_importInfo())
st.subheader("History")

# Get all labs from db to select one
db = paths.DB
lab = radio_lab(db, extra_options=["All"])
lab_dir = paths.LAB_DIRS[lab]
csv = os.path.join(lab_dir, "elog.csv")

# Read CSV file
try:
    df = pd.read_csv(csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
except FileNotFoundError:
    st.warning(f"Error: File '{csv}' not found.")
    st.stop()

# Select microscope first
st.subheader("Select Microscope")
selected_microscope = dfselect_mic(df)
# Microscope logbook
st.write(df[df['Microscope'] == selected_microscope])

# Fetch last entry as defaults
lastEntry = get_lastEntry(df, selected_microscope)

message_id = int(lastEntry.get('Message ID', 0)) + 1  # Increment Message ID
message_id_raw = lastEntry.get('Message ID', 0)
message_id = int(message_id_raw) + 1 if pd.notna(message_id_raw) else 1  # Increment Message ID
previous_plateid = int(lastEntry.get('PlateID', "Unknown"))
previous_author = str(lastEntry.get('Author', "Unknown"))
scanning_path = str(lastEntry.get('Scanning Path', ""))

# Extract RUN_W_B
match = re.search(r'RUN\d+_W\d+_B\d+', scanning_path)
if match:
    run_w_b = match.group(0)  # Full match, e.g., RUN2_W1_B1
    # Split to extract RUN
    parts = run_w_b.split('_')
    run = parts[0][3:]  # Extract the number after 'RUN'
else:
    run_w_b = "Unknown"
    run = "Unknown"
# Extract plate directory, e.g., P001
plate_dir = scanning_path.split("\\")[-1] if "\\" in scanning_path else "Unknown"

emu_type = lastEntry.get('EmuType', "Nagoya")
scanning_status = lastEntry.get('Scanning', "IN PROGRESS")
area = lastEntry.get('Area', "")
delete_obx = lastEntry.get('DeleteObx', "no")
# Add new entry form
with st.form("New Entry"):
    st.subheader(f"New Entry {run_w_b}, Plate: {plate_dir}")
    st.warning(f"Under development. Cannot submit yet.")

    # Current date as a fixed field
    date_time = datetime.now().strftime("%a %d %b %Y %H:%M:%S %z")
    date = st.text_input("Current Date", value=date_time, disabled=True)  # Fixed field for current date
    plate_id = st.text_input("PlateID", value=previous_plateid)
    author = st.text_input("Author", value=previous_author)

    # Thickness calculation inputs
    col1, col2, col3, col4 = st.columns(4)
    with col1:
        mic_glass = st.number_input("mic@glass", value=0.0, step=0.1)
    with col2:
        mic_bot = st.number_input("mic@bot", value=0.0, step=0.1)
    with col3:
        mic_base = st.number_input("mic@base", value=0.0, step=0.1)
    with col4:
        mic_top = st.number_input("mic@top", value=0.0, step=0.1)

    # Recalculate dynamically updated thickness values
    glass, bottom, base, top = calculate_thickness(mic_glass, mic_bot, mic_base, mic_top)

    # Display dynamically updated thickness values
    col1, col2, col3, col4 = st.columns(4)
    with col1:
        st.text_input("Glass", value=str(glass), disabled=True)
    with col2:
        st.text_input("Bottom", value=str(bottom), disabled=True)
    with col3:
        st.text_input("Base", value=str(base), disabled=True)
    with col4:
        st.text_input("Top", value=str(top), disabled=True)

    # File uploader for emulsion photo
    uploaded_file = st.file_uploader("Import damaged emulsion", type=["png", "jpg", "jpeg"])

    # Other fields
    scanning_path = st.text_input("Scanning Path", value=scanning_path)
    emu_type = st.radio("EmuType", ["Nagoya", "Slavich"], index=0 if emu_type == "Nagoya" else 1, horizontal=True)
    scanning_status = st.radio("Scanning", ["IN PROGRESS", "DONE", "TO RESCAN"], index=1 if scanning_status == "IN PROGRESS" else 0, horizontal=True)
    area = st.text_input("Area", value=area)
    delete_obx = st.radio("DeleteObx", ["no", "DONE"], index=1 if delete_obx == "no" else 0, horizontal=True)
    note = st.text_area("Note", value="Scanning time: ")

    # Submit button inside the form
    submitted = st.form_submit_button("Submit")

    # On submit, append to CSV
    if submitted:
        if uploaded_file:
            # Save uploaded file to the lab directory or a subfolder
            file_path = os.path.join(paths.QUALITY_CHEKS_DIR,run,run_w_b,plate_dir,uploaded_file.name)
            with open(file_path, "wb") as f:
                f.write(uploaded_file.getbuffer())
            st.success(f"Image '{uploaded_file.name}' uploaded successfully to {file_path}")

        new_row = {
            "Message ID": message_id,
            "Date": date_time,
            "PlateID": plate_id,
            "Author": author,
            "Glass": glass,
            "Bottom": bottom,
            "Base": base,
            "Top": top,
            "Scanning Path": scanning_path,
            "EmuType": emu_type,
            "Scanning": scanning_status,
            "Area": area,
            "Microscope": selected_microscope,
            "Processing": "no",
            "DeleteObx": delete_obx,
            "Linking": "no",
            "Threshold": "2000",
            "Note": note
        }

        # Append to CSV
        #df = pd.concat([df, pd.DataFrame([new_row])], ignore_index=True)
        #df.to_csv(csv, index=False)

        st.success(f"New entry with Message ID {message_id} added successfully!")
        st.experimental_rerun()