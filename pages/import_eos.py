import os
import paths
import streamlit as st
import settings 
import pwd, grp
import pandas as pd
from src.utils import write_permissions, is_valid_password, get_file_modified_time, traverseEOS_scanningData, split_csv

eos_csv = paths.EOS_CSV
search_file = settings.TRACKS_FILE
search_path = paths.EOS_EMU_DIR
group_name = settings.GROUP_NAME

time = get_file_modified_time(eos_csv)
st.caption(f'_Latest import_ :blue[{time}]')

# Credentials
#st.text(f"User: {os.getenv('USER')}")
#st.text(f"Username: {os.getenv('USERNAME')}")
#st.subheader(f"Credentials")
#st.text(f"sso_clientID: {os.getenv('sso_clientID')}")
#st.text(f"sso_clientSecret: {os.getenv('sso_clientSecret')}")
#raw_token = os.getenv('sso_clientSecret')
#decoded_token = jwt.decode(raw_token, options={"verify_signature": False})
#st.text(f"Decoded token: {decoded_token}")
#st.text(json.dumps(decoded_token, indent=4)) # log decoded token for debugging purposes
#roles = decoded_token.get('cern_roles')
#st.text(f"Roles: {roles}")
# Retrieve user from environment variable set by Nginx
#user = os.getenv('HTTP_X_FORWARDED_USER')
#remote_user = os.getenv('HTTP_X_REMOTE_USER')
#st.subheader("Environment Variables")
#st.text(f"X-Forwarded-User: {user}")
#st.text(f"X-Remote-User: {remote_user}")


###################################
###################################
######### Traverse EOS! ###########
###################################
###################################

# Restrict access with password 
with open(paths.EOS_IMPORT_HASH_TXT, 'r') as file:
    correct_password_hash = file.read().strip()
password = st.text_input("Enter Password:", type="password")

# Scanning data
st.subheader(f"Scanning data")
if st.button("Analyze", key="scanning"):
    if is_valid_password(password, correct_password_hash):
        with st.spinner("Analyzing EOS space. This will take some minutes...)"):
            eos_csv=traverseEOS_scanningData(csv_path=eos_csv, search_path=search_path)
            st.success(f"Finished traversing EOS. Data is saved: {eos_csv}.")
            # Split global csv into individual csv for each lab
            try:
                constructed_paths = split_csv(eos_csv)
                st.success("Constructed CSVs:")
                for path in constructed_paths:
                    st.write(path)
            except Exception as e:
                st.warning(f"Error during CSV splitting: {e}")
                st.stop()
    else:
        st.error("Incorrect password. Access denied.")


st.subheader(f"Reconstruction data")
# Reconstruction data
if st.button("Analyze", key="reco"):
    if is_valid_password(password, correct_password_hash):
        with st.spinner("Analyzing EOS space..."):
            #traverse_reconstructionData(csv_path=reconstruction_csv, search_path=search_path)
            st.warning(f"Not available yet.")
            #st.success(f"Finished traversing EOS. Information is saved to the csv {reconstruction_csv}.")
    else:
        st.error("Incorrect password. Access denied.")

st.info('Analyze will traverse the EOS space', icon="ℹ️")
