import streamlit as st
import paths
import settings
import pandas as pd
import os
from src import selections
from src.df_visualizations import EOSvsELOG, ELOGtime
from src.utils import db_to_df_runinfo, unique_column_combinations, unique_column_values, get_file_modified_time, get_importInfo, generate_html_table_from_csv

st.caption(get_importInfo())

# Get all labs from db to select one
db = paths.DB
lab = selections.radio_lab(db, extra_options=["All"])
lab_dir = paths.LAB_DIRS[lab]
elog_csv = os.path.join(lab_dir,"elog.csv")
eos_csv = os.path.join(lab_dir,"eos.csv")
try:
    df = pd.read_csv(elog_csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)

except FileNotFoundError: 
    st.warning(f"Error: File '{elog_csv}' not found in {lab_dir}.")
    st.stop()
try:
    df = pd.read_csv(eos_csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
except FileNotFoundError:
    st.warning(f"Error: File '{eos_csv}' not found in {lab_dir}.")
    st.stop()

# Generate global HTML table
html_table = generate_html_table_from_csv(elog_csv, eos_csv)
st.markdown(html_table, unsafe_allow_html=True)

######## DATABASE DATA #########
dfDB = db_to_df_runinfo(db)
########## ELOG DATA ###########
dfELOG = pd.read_csv(elog_csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)
########### EOS DATA ###########
dfEOS = pd.read_csv(eos_csv, na_values=settings.NA_VALUES, keep_default_na=False, encoding=settings.DECODING)

# Filter the dataframes on selections
dfs = [dfELOG, dfEOS]
dfs = selections.dfselect(dfs,'Run')
dfs = selections.dfselect(dfs,'Wall')
dfs = selections.dfselect(dfs,'Run_Wall_Brick')
dfELOG = dfs[0]
dfEOS = dfs[1]


# Visualize
st.subheader('EOSvsELOG')
EOSvsELOG(dfELOG, dfEOS, dfDB)
st.info('- Pink dashed line: percentage reference (varies based on the assigned plates / brick e.g. 60). \n - Percentage on bars: ELOG data percentage. \n- Hover on bars: both EOS and ELOG percentages.', icon="ℹ️")
st.subheader('Progress accross time')
st.info('Previous selections apply', icon="👆")
dt = st.radio('Choose dt',['Day','Week','Month',"Year"], index=1)
ELOGtime(dfELOG, time_col=dt)


# Choose source dataframe
st.subheader('Assigned Bricks per Lab')
source = st.radio('Source',['ELOG', 'EOS', 'Merged'])
if source == 'ELOG':
    dfSource = dfELOG
elif source == 'EOS':
    dfSource = dfEOS
elif source == "Merged":
    dfSource = pd.concat([dfEOS, dfELOG], ignore_index=True)
else:
    raise ValueError('Unkown source')

# Assigned Bricks per Lab
columns=['Lab','Run_Wall_Brick','Run']
dfAssigned = unique_column_combinations(dfSource, columns)
grouped = dfAssigned.groupby(['Lab', 'Run'])['Run_Wall_Brick'].apply(lambda x: '\n'.join(x)).reset_index()
pivot_table = grouped.pivot(index='Lab', columns='Run', values='Run_Wall_Brick')
pivot_table = pivot_table.fillna('')
st.write(pivot_table.to_html(escape=False).replace("\\n", "<br>"), unsafe_allow_html=True)


# Assigned Bricks per Lab
columns=['Lab','Run_Wall_Brick','Run','Wall']
dfAssigned = unique_column_combinations(dfSource, columns)
labs = unique_column_values(dfSource, 'Lab')
lab_dfs = {lab: dfAssigned[dfAssigned['Lab'] == lab] for lab in labs}
for lab, df in lab_dfs.items():
    st.subheader(lab)
    grouped = df.groupby(['Run', 'Wall'])['Run_Wall_Brick'].apply(lambda x: '\n'.join(x)).reset_index()
    pivot_table = grouped.pivot(index='Run', columns='Wall', values='Run_Wall_Brick')
    pivot_table = pivot_table.fillna('')
    st.write(pivot_table.to_html(escape=False).replace("\\n", "<br>"), unsafe_allow_html=True)

