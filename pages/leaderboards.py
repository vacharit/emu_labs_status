import streamlit as st
import settings, paths
import os
from src.utils import generate_leaderboards, highlight_leaderboards
from src.selections import radio_lab

# Get all labs from db to select one
db = paths.DB
lab = radio_lab(db, extra_options=["All"])
lab_dir = paths.LAB_DIRS[lab]
csv = os.path.join(lab_dir,"elog.csv")


if lab is not None:
    try:
        # Load the CSV data
        leaderboards = generate_leaderboards(csv)

        # Display the leaderboards
        st.header("Fastest Microscope")
        st.dataframe(leaderboards["Microscopes"].style.apply(highlight_leaderboards, axis=1))

        st.header("Overall scans")
        st.dataframe(leaderboards["Overall"].style.apply(highlight_leaderboards, axis=1))

        st.header('Weekly scans')
        st.dataframe(leaderboards["Weekly"].style.apply(highlight_leaderboards, axis=1))
    except FileNotFoundError:
        st.warning(f"Error: File '{csv}' not found.")
        st.stop()