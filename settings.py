import os
import re


# Module to save important configuration for the application
PROJECT_NAME='EmuProgress'
GROUP_NAME='sndlhc-cg' 
LABORATORIES={'CERN':'CR', 'Napoli':'NA', 'Bologna':'BO','Nagoya':'NAG','Lebedev':'LEB'}
LAB_EOS_DIRS=['Bologna', 'CERN', 'Napoli', 'Lebedev', 'Nagoya']
YEAR_EOS_DIRS=['2022', '2023']
USER = os.getenv('USER')
USERNAME = os.getenv('USERNAME')
SCANPATH_TO_UPPER = True
SCANPATH_REPLACE_BACKSLASH = True
DECODING = 'ISO-8859-1' #UTF-8 won't work. Only ISO-8859-1 or WINDOWS-1252.
ENABLE_LOGBOOK_REFRESH_BUTTON = False
# PATTERNS FOR EOS TRAVERSING
PLATE_PATTERN_STRICT = re.compile(r'^p\d{3}$', re.IGNORECASE) 
PLATE_PATTERN_FLEX = re.compile(r'(P?\d+)(_RESCAN)?', re.IGNORECASE)
BRICK_PATTERN_STRICT = re.compile(r'^b\d{6}$', re.IGNORECASE) 
RUN_PATTERN_FLEX = re.compile(r'RUN\d+', re.IGNORECASE) 
RUN_WALL_BRICK_PATTERN_FLEX = re.compile(r'.*RUN\d+_W\d+_B\d+.*', re.IGNORECASE) 
RUN_WALL_BRICK_PATTERN_STRICT = re.compile(r'RUN(\d+)_W(\d+)_B(\d+)', re.IGNORECASE) 
MIC_PATTERN = re.compile(r'(?:^|/|_)(mic\d+|MIC\d+)', re.IGNORECASE)
TRACKS_FILE = "tracks.raw.root"
# DATABASE TABLES
LABPROGRESS_TABLE = 'LabProgress'
RUNINFO_TABLE = 'RunInfo'
SCANNINGEOS_TABLE = 'ScanningEOS'
RECOEOS_TABLE = 'RecoEOS'
SUMMARY_SCANNINGEOS_TABLE = 'SummaryScanningEOS'
SUMMARY_LABPROGRESS_TABLE= 'SummaryLabProgress'
SUMMARY_RUNINFO_TABLE = 'SummaryRunInfo'
SUMMARY_MONTHLY_TABLE = 'SummaryMonthly'
SUMMARY_WEEKLY_TABLE = 'SummaryWeekly'
# MISC
NA_VALUES = ['','NULL', 'NaN', '-1.#IND', '1.#IND', '#N/A', '#N/A N/A', '-NaN', '-nan'] # Custom nans to block interpretation of "NA" i.e. napoli lab, as a nan

