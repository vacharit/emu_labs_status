## Application

https://sndlhc-emuprogress.app.cern.ch/

## To run & develop locally

pyenv install 3.11.7
pyenv local 3.11.7

python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
streamlit run app.py

## TODO

- [x] EOSvsELOG for scanning data
- [ ] EOSvsELOG for reconstruction data
- [ ] EOSvsELOG entry by entry more reliable
- [x] Access to eos
- [x] Automatic logbook uploads
- [x] Automatic global logbook
- [x] Automatic redeployment every ~15 mins
- [x] Import ELOG .csv file to get recent progress report  
- [x] Import EOS data to get recent state 
- [x] Trigger EOS update with password (limit traversing)  
- [x] Manage duplicate entries in elog. Make sure no plates are counted twice (e.g Search for directories like 'p041_rescan' and if p041 exists then count only once). 
- [x] Manage duplicate entries in EOS. Make sure no plates are counted twice (e.g Search for directories like 'p041_rescan' and if p041 exists then count only once). 
- [x] When analyzing EOS space, check first if the directories traversed are modified from the last analysis. IF not skip to save time
### Optional 

- [x] Routing to subdomain snd-lhc-monitoring.web.cern.ch/ for consistency. Used html iframe
- [x] Leaderboard
- [ ] Integrate redeployment in CI workflow

### Issues
- [ ] Encoding-Decoding of ELOG .csv file. 'There are characters in your file that cannot be decoded using the UTF-8 encoding.' Used ISO-8859-1
