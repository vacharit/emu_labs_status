# Use a slim Python 3.9 image
FROM python:3.9-slim

# Set working directory
WORKDIR /app

# Install minimal system dependencies 
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl \
    wget \
    unzip \
    libxrender1 \
    libxext6 \
    libx11-6 \
    libgtk-3-0 \ 
    libxcomposite1 \
    libxrandr2 \
    libxt6 \
    fonts-liberation \
    && rm -rf /var/lib/apt/lists/*
    #libdbus-glib-1-2 \ #Firefox ESR with Geckodriver
    #libnss3 \
    #firefox-esr \
    #libasound2 \ 
    #libgl1-mesa-glx \
    #libpci3 \
    
# Install Geckodriver for Selenium
#RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.35.0/geckodriver-v0.35.0-linux64.tar.gz \
#    && tar -xzf geckodriver-v0.35.0-linux64.tar.gz \
#    && mv geckodriver /usr/local/bin/ \
#    && chmod +x /usr/local/bin/geckodriver \
#    && rm geckodriver-v0.35.0-linux64.tar.gz
# Verify Geckodriver installation
#RUN geckodriver --version
# Make writable cache directories for caching fonts
#RUN mkdir -p /tmp/fonts && chmod 777 /tmp/fonts
#ENV FONTCONFIG_PATH=/etc/fonts
#ENV FONTCONFIG_CACHE=/tmp/fonts

# Copy application files
COPY . .

# Install Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Expose Streamlit port
EXPOSE 8501

# Start Streamlit app
ENTRYPOINT ["streamlit", "run", "app.py"]
